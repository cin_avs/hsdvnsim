# HSDVNSim: A new Omnet-based framework to simulate Hierarchical SDN solutions in vehicular networks.

## Getting Started

* Using openflow communication messages;
* Sumo mobility model;
* Veins framework to vehicle modules;
* Inet framework to network implementations;
* more

### Prerequisites

* [Omnet 5.1.1](https://www.omnetpp.org/omnetpp) - OMNeT++ is an extensible, modular, component-based C++ simulation library and framework, primarily for building network simulators.
* [Sumo 0.30.0](http://sumo.dlr.de/wiki/Downloads) - Road traffic simulation package designed to handle large road networks
* [Veins Framework](http://veins.car2x.org/download/) - Used to VANETs scenarios

## Authors

* **Alexsanderson Santos** - *avs@cin.ufpe.br*
* **Francisco Junior** - ffmj@cin.ufpe.br
* **Kelvin Dias** - *kld@cin.ufpe.br*
