#include "CentralController_AL.h"
#include "messages/StatusMsg_m.h"
#include "messages/OpenFlowMsg_m.h"
#include "messages/MessageStructures.h"
#include <unordered_map>
#include "structures/CarInformations.h"
#include "structures/LCinformations.h"
#include "structures/openflow.h"
#include "inet/common/ModuleAccess.h"
#include "inet/mobility/contract/IMobility.h"
#include "structures/Table.h"
#include "structures/FlowTable.h"

Define_Module(CentralController_AL);

void CentralController_AL::initialize()
{
    printLocalControllerPositions = par("printLocalControllerPositions");
}

void CentralController_AL::handleMessage(cMessage *msg)
{
    std::string verify (msg->getClassName());

    if(verify.compare("StatusMsg") == 0){
        StatusMsg *status = check_and_cast<StatusMsg *>(msg);
        switch(status->getMsgType()){
            //case OF_REQUEST_LC_ADDR: //CentralController
            //    processOFRLA(status);
            //    break;
            /*
             * TODO
             */
            default:
                std::cout << "Unknown message type: " << status->getName() << "," << status->getMsgType() << std::endl;
                break;
        }
    }
    else if(verify.compare("OpenFlowMsg") == 0){
        OpenFlowMsg *ofm = check_and_cast<OpenFlowMsg *>(msg);
        switch(ofm->getOFtype()){
            case OFPT_CONTROLLER_STATUS: //Central Controller: "Local Controlers Positions"
                insertLocalControllerStatus(ofm);
                break;
            case OFPT_GET_CONFIG_REQUEST: //Central Controller: "Send Localcontrollers Positions to Cars"
                sendBackLocalControllerInfos(ofm);
                break;
        }
    }
    else{
        EV << "Unknown Message: " << msg->getClassName() << std::endl;
        std::cout << "Unknown Message: " << msg->getClassName() << std::endl;
        delete msg;
    }
}


/*
 * Methods to process OpenFlowMsg
 */

void CentralController_AL::insertLocalControllerStatus(OpenFlowMsg *status)
{
    LCinformations l;
    string id = status->getNodeName();
    char c = id[2]; //getting id from message Ex: lC1 -> 1
    int idValue = c - '0'; //converting char to int
    l.idLC = idValue;
    l.address = status->getSourceAddress();
    l.port = status->getSourcePort();
    l.x = status->getPosX();
    l.y = status->getPosY();
    l.z = status->getPosZ();
    insertPositionInMap(idValue , l , lc_positions);

    sendToGlobalViewApp(status);
}

void CentralController_AL::sendToGlobalViewApp(OpenFlowMsg *ofm)
{
    StatusMsg *pkt = new StatusMsg("LocalViewPositions");
    pkt->setMsgType(LC_POSITIONS_TO_UDPAPP);
    pkt->setNodeName(ofm->getNodeName());
    pkt->setSourceAddress(ofm->getSourceAddress());
    pkt->setSourcePort(ofm->getSourcePort());
    pkt->setPosX(ofm->getPosX());
    pkt->setPosY(ofm->getPosY());
    pkt->setPosZ(ofm->getPosZ());

    send(pkt, "udpdataOut");
}

void CentralController_AL::sendBackLocalControllerInfos(OpenFlowMsg *status)
{
    OpenFlowMsg *msg = check_and_cast<OpenFlowMsg *>(status);
    OpenFlowMsg *response = new OpenFlowMsg("LocalControllers Informations");

    response->setOFtype(OFPT_GET_CONFIG_REPLY);
    response->setDestAddress(msg->getSourceAddress());
    response->setDestPort(msg->getSourcePort());
    response->setInfo(getLCPositions());

    send(response, "tcpdataOut");
}

/*
 *Complement to process methods OpenFlowMsg
 */

localControllerInformations CentralController_AL::getLCPositions()
{
    localControllerInformations infos;

    for (auto& kv : lc_positions) {
        int id = kv.first;
        //printf("Local Controller - %i\t-\t", id);
        LCinformations value = lc_positions[id];
        infos.id.push_back(value.getID_LC());
        infos.address.push_back(value.getAddress_LC());
        infos.port.push_back(value.getPort_LC());
        infos.posX.push_back(value.getPOSX_LC());
        infos.posY.push_back(value.getPOSY_LC());
        infos.posZ.push_back(value.getPOSZ_LC());
     }
    return infos;
}

void CentralController_AL::insertPositionInMap(int id, LCinformations p, unordered_map< int, LCinformations > &someMap ){
    auto result = someMap.emplace(id, p);
    if (!result.second){
        someMap[id] = p;
    }

    if(printLocalControllerPositions == true){
        printHashMap(lc_positions);
    }
}

void CentralController_AL::printHashMap(unordered_map< int, LCinformations > someHashMap){

    printf("********** Local Controllers Infos*************\n");
    for (auto& kv : someHashMap) {
        int id = kv.first;
        printf("Local Controller - %i\t-\t", id);
        LCinformations value = someHashMap[id];
        string strValue = value.printInformations();
        printf("%s\n", strValue.c_str());
    }
    printf("**********************************************\n");
}
