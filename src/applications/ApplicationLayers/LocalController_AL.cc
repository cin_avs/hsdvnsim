#include "LocalController_AL.h"
#include "messages/StatusMsg_m.h"
#include "messages/OpenFlowMsg_m.h"
#include "messages/MessageStructures.h"
#include <unordered_map>
#include "structures/CarInformations.h"
#include "structures/LCinformations.h"
#include "structures/openflow.h"
#include "inet/common/ModuleAccess.h"
#include "inet/mobility/contract/IMobility.h"
#include "structures/Table.h"
#include "structures/FlowTable.h"

Define_Module(LocalController_AL);

void LocalController_AL::initialize(){/*TODO*/}

void LocalController_AL::handleMessage(cMessage *msg)
{
    std::string verify (msg->getClassName());

    if(verify.compare("StatusMsg") == 0){
        StatusMsg *status = check_and_cast<StatusMsg *>(msg);
        switch(status->getMsgType()){
        //case VEHICLE_STATUS_POSITION:  //LocalController
        //    processVSP(status);
        //    break;
        default:
            std::cout << "Unknown message type: " << status->getName() << "," << status->getMsgType() << std::endl;
            break;
        }
    }
    else if(verify.compare("OpenFlowMsg") == 0){
        OpenFlowMsg *ofm = check_and_cast<OpenFlowMsg *>(msg);
        switch(ofm->getOFtype()){
        case OFPT_QOS_REQUEST: //---> LocalControllers: "Send priority to Cars"
            processFeaturesRequest(ofm);
            break;
        }
    }
    else{
        EV << "Unknown Message: " << msg->getClassName() << std::endl;
        std::cout << "Unknown Message: " << msg->getClassName() << std::endl;
        delete msg;
    }
}

/*
 * Methods to process StatusMsg
 */

//void LocalController_AL::processVSP(StatusMsg *status)
//{
//    CarInformations p;
//    string idcar = status->getNodeName();
//    char idcar2 = idcar[4];
//    int idcar3 = idcar2 - '0';
//    p = getPositionFromLine(idcar3, status);
//    insertPositionInMap(idcar3 , p , nV_position);
//    printHashMap(nV_position);
//}

/*
 * Methods to process OpenFlowMsg
 */

void LocalController_AL::processFeaturesRequest(OpenFlowMsg *ofm)
{
    int priority = verifyClusters(ofm);

    ofp_flow_table_header_fields rule;
    rule.sourcePort = ofm->getSourcePort();
    rule.srcMAC = "-";
    rule.dstMAC = "-";
    rule.vlanId = 0;
    rule.priority = 100;
    rule.ethernetType = 0;
    rule.srcIP = "-";
    rule.dstIP = "-";
    rule.ipProto = "-";
    rule.srcTcpPort = ofm->getSourcePort();
    rule.action = priority;
    rule.counter = 1;

    OpenFlowMsg *response = new OpenFlowMsg("QoS_Reply");
    response->setOFtype(OFPT_QOS_REPLY);
    response->setRule(rule);
    response->setAppType(ofm->getAppType());
    send(response, "tcpdataOut");
}

/*
 * Complement to process methods OpenFlowMsg
 */

int LocalController_AL::verifyClusters(OpenFlowMsg *ofm)
{
    int appType = ofm->getAppType();
    int priority = OFPT_PRIORITY_NOTHING;

    string nodeName = ofm->getNodeName();

    if((appType == 1) && ag7.size() < 3){
        ag7.push_back(nodeName);
        priority = OFPT_PRIORITY_SEVEN;
        //printAggregationMap();
    }

    else if((appType == 1) && ag6.size() < 3){
        ag6.push_back(nodeName);
        priority = OFPT_PRIORITY_SIX;
        //printAggregationMap();
    }
    else if((appType == 1) && ag5.size() < 3){
        ag5.push_back(nodeName);
        priority = OFPT_PRIORITY_FIVE;
        //printAggregationMap();
    }

    else if((appType == 1) && ag4.size() < 3){
        ag4.push_back(nodeName);
        priority = OFPT_PRIORITY_FOUR;
        //printAggregationMap();
    }
    else if((appType == 1) && ag3.size() < 3){
        ag3.push_back(nodeName);
        priority = OFPT_PRIORITY_THREE;
        //printAggregationMap();
    }

    else if((appType == 2 || appType == 3) && ag1.size() < 3){
        ag2.push_back(nodeName);
        priority = OFPT_PRIORITY_ONE;
        //printAggregationMap();
    }

    else if((appType == 2 || appType == 3) && ag0.size() < 50){
        ag1.push_back(nodeName);
        priority = OFPT_PRIORITY_NOTHING;
        //printAggregationMap();
    }

    else{
        ag0.push_back(nodeName);
        //printAggregationMap();
    }
    return priority;
}

void LocalController_AL::printAggregationMap()
{
    std::cout << "Aggregation 7" << std::endl;
    for(auto &i : ag7){
        cout << i << ", ";
    }

    cout << "\n";
    std::cout << "Aggregation 6" << std::endl;
    for(auto &i : ag6){
        cout << i << ", ";
    }

    cout << "\n";
    std::cout << "Aggregation 5" << std::endl;
    for(auto &i : ag5){
        cout << i << ", ";
    }

    cout << "\n";
    std::cout << "Aggregation 4" << std::endl;
    for(auto &i : ag4){
        cout << i << ", ";
    }

    cout << "\n";
    std::cout << "Aggregation 3" << std::endl;
    for(auto &i : ag3){
        cout << i << ", ";
    }

    cout << "\n";
    std::cout << "Aggregation 2" << std::endl;
    for(auto &i : ag2){
        cout << i << ", ";
    }

    cout << "\n";
    std::cout << "Aggregation 1" << std::endl;
    for(auto &i : ag1){
        cout << i << ", ";
    }

    cout << "\n";
    std::cout << "Aggregation 0" << std::endl;
    for(auto &i : ag0){
        cout << i << ", ";
    }
}

/*
 * Complement to process methods StatusMsg
 */

//CarInformations LocalController_AL::getPositionFromLine(int id, StatusMsg *status)
//{
//    CarInformations p;
//    p.idCar = id;
//    p.nameCar = status->getNodeName();
//    p.addressCar = status->getSourceAddress();
//    p.x = status->getPosX();
//    p.y = status->getPosY();
//    p.z = status->getPosZ();
//    return p;
//}

//void LocalController_AL::insertPositionInMap(int id, CarInformations p, unordered_map< int, CarInformations > &someMap )/
//{
//    std::string verify = p.getNameCar().c_str();
//    auto result = someMap.emplace(id, p);
//    if (!result.second){
//       someMap[id] = p;
//    }
//    printHashMap(nV_position);
//}

//void LocalController_AL::printHashMap(unordered_map< int, CarInformations > someHashMap)
//{
//    printf("********** Network View Informations**********\n");
//    for (auto& kv : someHashMap) {
//        int id = kv.first;
//        printf("Car - %i\t-\t", id);
//        CarInformations value = someHashMap[id];
//        string strValue = value.printInformations();
//        printf("%s\n", strValue.c_str());
//    }
//    printf("**********************************************\n");
//}
