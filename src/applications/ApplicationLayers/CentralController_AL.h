#ifndef __CentralController_AL_H
#define __CentralController_AL_H

#include <string.h>
#include <omnetpp.h>
#include "messages/StatusMsg_m.h"
#include "messages/OpenFlowMsg_m.h"
#include <unordered_map>
#include "structures/CarInformations.h"
#include "structures/LCinformations.h"
#include "messages/MessageStructures.h"
#include "structures/Table.h"
#include "structures/FlowTable.h"

using namespace omnetpp;


class CentralController_AL : public cSimpleModule
{

protected:
    unordered_map< int, LCinformations> lc_positions;
    bool printLocalControllerPositions;

protected:
    virtual void initialize() override;
    virtual void handleMessage(cMessage *msg) override;

protected:
    //virtual void processOFRLA(StatusMsg *status);
    virtual void insertLocalControllerStatus(OpenFlowMsg *status);
    virtual void sendBackLocalControllerInfos(OpenFlowMsg *status);
    virtual void sendToGlobalViewApp(OpenFlowMsg *ofm);
    virtual localControllerInformations getLCPositions();
    virtual void insertPositionInMap(int id, LCinformations p, unordered_map< int, LCinformations > &someMap );
    virtual void printHashMap(unordered_map< int, LCinformations > someHashMap);
};

#endif
