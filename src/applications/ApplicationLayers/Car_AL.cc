#include "Car_AL.h"
#include "messages/StatusMsg_m.h"
#include "messages/OpenFlowMsg_m.h"
#include "messages/MessageStructures.h"
#include <unordered_map>
#include "structures/CarInformations.h"
#include "structures/LCinformations.h"
#include "structures/openflow.h"
#include "inet/common/ModuleAccess.h"
#include "inet/mobility/contract/IMobility.h"
#include "structures/Table.h"
#include "structures/FlowTable.h"

Define_Module(Car_AL);

void Car_AL::initialize(){/*TODO*/}

void Car_AL::handleMessage(cMessage *msg)
{
    std::string verify (msg->getClassName());

    if(verify.compare("StatusMsg") == 0){
        StatusMsg *status = check_and_cast<StatusMsg *>(msg);

        switch(status->getMsgType()){
            case LC_CHOICE_BY_CAR:   //Cars
                processLCBC(status);
                break;
            case STATUS_QOS_REQUEST: //Cars:
                processFlowTableRequest(status);
                break;
            case CC_LOCALCONTROLLER_CHANGE:
                //localControllerChoice = status->getDestAddress();
                //localControllerPortChoice = status->getDestPortTCP();
                processLCBC(status);
                break;
            default:
                std::cout << "Unknown message type: " << status->getName() << "," << status->getMsgType() << std::endl;
                break;
        }
    }
    else if(verify.compare("OpenFlowMsg") == 0){
        OpenFlowMsg *ofm = check_and_cast<OpenFlowMsg *>(msg);
        switch(ofm->getOFtype()){
            case OFPT_QOS_REPLY:  //car recebe prioridade
                keepingPriority(ofm);
                break;
        }

    }
    else if(verify.compare("VideoRTPMessage") == 0){
        send(msg, "rtpInterfaceOut");
    }
    else{
        EV << "Unknown Message: " << msg->getClassName() << std::endl;
        std::cout << "Unknown Message: " << msg->getClassName() << std::endl;
        delete msg;
    }
}

/*
 * Methods to process StatusMsg
 */

void Car_AL::processLCBC(StatusMsg *status)
{
    localControllerChoice = status->getDestAddress();
    localControllerPortChoice = status->getDestPortTCP();

    //Send to class CarPositionsApp and others
    if(status->getMsgType() != CC_LOCALCONTROLLER_CHANGE){
        StatusMsg *response = new StatusMsg("LocalControllerAddressData");
        response->setMsgType(LC_ADDRESS_DATA);
        response->setDestAddress(localControllerChoice.c_str());
        response->setDestPort(localControllerPortChoice);
        send(response, "udpdataOut");
    }

}

void Car_AL::processFlowTableRequest(StatusMsg *status)
{
    if(createTM){
        createTableMiss(status->getSourcePort(), status->getSourceAddress());
        createTM = false;
    }

    Table t;
    StatusMsg *response = new StatusMsg("QoS_Rule");
    ofp_flow_table_header_fields rule;
    t = flowTable.getTable(countFT);
    countFT++;
    rule = t.getRule(OF_TABLE_MISS);
    response->setMsgType(STATUS_TABLE_MISS);
    response->setDestAddress(rule.dstIP.c_str());
    response->setDestPortTCP(rule.dstTcpPort);
    send(response, "tcpdataOut");
}

/*
 * Complement Methods to StatusMsg process
 */

void Car_AL::createTableMiss(int sourcePort, string sourceAddress)
{
    ofp_flow_table_header_fields match;
    match.sourcePort    = sourcePort;
    match.srcMAC        = "-";
    match.dstMAC        = "-";
    match.vlanId        = 0;
    match.priority      = 1;
    match.ethernetType  = 0;
    match.srcIP         = sourceAddress;
    match.dstIP         = localControllerChoice;
    match.ipProto       = "-";
    match.srcTcpPort    = sourcePort;
    match.dstTcpPort    = localControllerPortChoice;
    match.action        = OFPT_TABLE_MISS;
    match.counter       = count++;

    table.addTableEntry(OF_TABLE_MISS, match); //Adiciona TableMiss
    flowTable.addFlowEntry(countFT, table);
}


/*
 * Methods to process OpenFlowMsg
 */

void Car_AL::keepingPriority(OpenFlowMsg *ofm)
{
    //Table t;

    appType = ofm->getAppType();
    serverAddr = ofm->getDestAddress();
    serverPort = ofm->getDestPort();

    ofp_flow_table_header_fields rule;
    rule = ofm->getRule();
    table.addTableEntry(ofm->getAppType(), rule); //Adiciona Tabela com regra de QoS
    table.printTableEntry(); //PRINT DA TABELA ADICIONADA
    flowTable.addFlowEntry(countFT, table);

    //StatusMsg *response = new StatusMsg("QOS_APP");
    //response->setMsgType(QOS_APP);
    //response->setDestAddress(serverAddr.c_str());
    //response->setDestPort(serverPort);
    //response->setQosAction(rule.action);
    //send(response, "udpdataOut");

    if(appType == 1){
        StatusMsg *response = new StatusMsg("QOS_VIDEO");
        response->setMsgType(QOS_VIDEO);
        response->setDestAddress(serverAddr.c_str());
        response->setDestPort(serverPort);
        response->setQosAction(rule.action);
        //send(response, "udpdataOut");
        send(response, "rtpInterfaceOut");
    }else{
        StatusMsg *response = new StatusMsg("QOS_APP");
        response->setMsgType(QOS_APP);
        response->setDestAddress(serverAddr.c_str());
        response->setDestPort(serverPort);
        response->setQosAction(rule.action);
        send(response, "udpdataOut");
    }



}



