#ifndef __CAR_AL_H
#define __CAR_AL_H

#include <string.h>
#include <omnetpp.h>
#include "messages/StatusMsg_m.h"
#include "messages/OpenFlowMsg_m.h"
#include <unordered_map>
#include "structures/CarInformations.h"
#include "structures/LCinformations.h"
#include "messages/MessageStructures.h"
#include "structures/Table.h"
#include "structures/FlowTable.h"

using namespace omnetpp;


class Car_AL : public cSimpleModule
{

protected:
    int countFT = 1;
    int count = 0;
    bool createTM = true;
    string localControllerChoice;
    int localControllerPortChoice;
    Table table;
    FlowTable flowTable;
    string serverAddr;
    int serverPort;
    int appType;
protected:
    virtual void initialize() override;
    virtual void handleMessage(cMessage *msg) override;

protected:
    virtual void processLCBC(StatusMsg *status);
    virtual void processFlowTableRequest(StatusMsg *status);
    virtual void createTableMiss(int sourcePort, string sourceAddress);
    virtual void keepingPriority(OpenFlowMsg *ofm);

};

#endif
