#ifndef __LocalController_AL_H
#define __LocalController_AL_H

#include <string.h>
#include <omnetpp.h>
#include "messages/StatusMsg_m.h"
#include "messages/OpenFlowMsg_m.h"
#include <unordered_map>
#include "structures/CarInformations.h"
#include "structures/LCinformations.h"
#include "messages/MessageStructures.h"
#include "structures/Table.h"
#include "structures/FlowTable.h"

using namespace omnetpp;


class LocalController_AL : public cSimpleModule
{

protected:
    //unordered_map< int, CarInformations> nV_position;

    vector <string> ag7;
    vector <string> ag6;
    vector <string> ag5;
    vector <string> ag4;
    vector <string> ag3;
    vector <string> ag2;
    vector <string> ag1;
    vector <string> ag0;

protected:
    virtual void initialize() override;
    virtual void handleMessage(cMessage *msg) override;

protected:
    //virtual void processVSP(StatusMsg *status);
    virtual void processFeaturesRequest(OpenFlowMsg *ofm);
    //virtual CarInformations getPositionFromLine(int id, StatusMsg *status);
    //virtual void insertPositionInMap(int id, CarInformations p, unordered_map< int, CarInformations > &someMap );
    //virtual void printHashMap(unordered_map< int, CarInformations > someHashMap);
    virtual int verifyClusters(OpenFlowMsg *ofm);
    virtual void printAggregationMap();


};

#endif
