#ifndef __APPCONTROL_H
#define __APPCONTROL_H

#include <string.h>
#include <omnetpp.h>
#include "messages/StatusMsg_m.h"
#include "messages/OpenFlowMsg_m.h"
#include <unordered_map>
#include "structures/CarInformations.h"
#include "structures/LCinformations.h"
#include "messages/MessageStructures.h"
#include "structures/Table.h"
#include "structures/FlowTable.h"

using namespace omnetpp;


class AppControl : public cSimpleModule
{
    protected:
        bool createTM = true;
        int count = 0;
        int countFT = 1;
        int countP7 = 1;
        int countP6 = 1;
        int countP5 = 1;
        int countP4 = 1;
        int countP3 = 1;
        int countP2 = 1;
        int countP1 = 1;
        int countP0 = 1;
        unordered_map< int, CarInformations> nV_position;
        unordered_map< int, LCinformations> lc_positions;
        string lCAddrConn;
        int lCPortConn;
        double lessDistance = 0.0;
        localControllerInformations infos;

        string localControllerChoice;
        int localControllerPortChoice;

        vector <string> ag7;
        vector <string> ag6;
        vector <string> ag5;
        vector <string> ag4;
        vector <string> ag3;
        vector <string> ag2;
        vector <string> ag1;
        vector <string> ag0;

        string serverAddr;
        int serverPort;
        int appType;

        Table table;
        FlowTable flowTable;

    protected:
        virtual void initialize() override;
        virtual void handleMessage(cMessage *msg) override;
        virtual CarInformations getPositionFromLine(int id, StatusMsg *status);
        virtual void insertPositionInMap(int id, CarInformations p, unordered_map< int, CarInformations > &someMap );
        virtual void insertPositionInMap(int id, LCinformations p, unordered_map< int, LCinformations > &someMap );
        virtual void printHashMap(unordered_map< int, CarInformations > someHashMap);
        virtual void printHashMap(unordered_map< int, LCinformations > someHashMap);
    protected:
        virtual void insertLocalControllerStatus(OpenFlowMsg *status);
        virtual void sendBackLocalControllerInfos(OpenFlowMsg *status);
        virtual void processLCBC(StatusMsg *status);
        virtual void processOFRLA(StatusMsg *status);
        virtual void processVSP(StatusMsg *status);
    protected:
        virtual void testaCodigo(std::string msg);
        virtual localControllerInformations getLCPositions();
        virtual void createTableMiss(int sourcePort, string sourceAddress);
        virtual void processFlowTableRequest(StatusMsg *status);
        virtual int verifyClusters(OpenFlowMsg *ofm);
        virtual void processFeaturesRequest(OpenFlowMsg *ofm);
        virtual void keepingPriority(OpenFlowMsg *ofm);
        //virtual void getQoS(StatusMsg *msg);
        virtual void printAggregationMap();
};

#endif
