#ifndef __ServerApplicationUDP_H
#define __ServerApplicationUDP_H

#include "inet/common/INETDefs.h"
#include <string.h>
#include <omnetpp.h>
#include "inet/applications/base/ApplicationBase.h"
#include "inet/transportlayer/contract/udp/UDPSocket.h"

namespace inet {

/**
 * Consumes and prints packets received from the UDP module.
 *
 */
class ServerApplicationUDP : public ApplicationBase
{
  protected:
    enum SelfMsgKinds { START = 1, STOP };

    UDPSocket socket;
    int localPort = -1;
    L3Address multicastGroup;
    simtime_t startTime;
    simtime_t stopTime;
    cMessage *selfMsg = nullptr;

    cLongHistogram delayCountStats;
    cOutVector delayCountVector;

    int numReceived = 0;
    static simsignal_t rcvdPkSignal;

  public:
    ServerApplicationUDP() {}
    virtual ~ServerApplicationUDP();

  protected:
    virtual void processPacket(cPacket *msg);
    virtual void processStatistics(cMessage *msg);
    virtual void setSocketOptions();

  protected:
    virtual int numInitStages() const override { return NUM_INIT_STAGES; }
    virtual void initialize(int stage) override;
    virtual void handleMessageWhenUp(cMessage *msg) override;
    virtual void finish() override;
    virtual void refreshDisplay() const override;

    virtual void processStart();
    virtual void processStop();

    virtual bool handleNodeStart(IDoneCallback *doneCallback) override;
    virtual bool handleNodeShutdown(IDoneCallback *doneCallback) override;
    virtual void handleNodeCrash() override;
};

}

#endif

