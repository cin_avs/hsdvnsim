#ifndef __ApplicationUDP_H
#define __ApplicationUDP_H

#include <vector>

#include "inet/common/INETDefs.h"

#include "inet/applications/base/ApplicationBase.h"
#include "inet/transportlayer/contract/udp/UDPSocket.h"
#include "messages/StatusMsg_m.h"
#include "inet/common/ModuleAccess.h"
#include "inet/mobility/contract/IMobility.h"

namespace inet {

/**
 * UDP application. See NED for more info.
 */
class ApplicationUDP : public ApplicationBase
{
  protected:
    enum SelfMsgKinds { START = 1, SEND, STOP };

    // parameters
    std::vector<L3Address> destAddresses;
    int localPort = -1, destPort = -1;
    simtime_t startTime;
    simtime_t stopTime;
    const char *packetName = nullptr;
    const char *positionsPacketName = nullptr;

    // state
    UDPSocket socket;
    cMessage *selfMsg = nullptr;

    //Local and Central Controllers Address and Port

    const char *localAddrConn = nullptr;
    int localPortConn = 0;
    const char *centralAddrConn = nullptr;
    int centralPortConn = 0;

    int msgType;

    L3Address cAddr;

    bool rApp = true;

    int qos;
    L3Address serverAppAddress;
    int serverAppPort = 0;

    bool configuredAddress = false;

    // statistics
    int numSent = 0;
    int numReceived = 0;

    static simsignal_t sentPkSignal;
    static simsignal_t rcvdPkSignal;

  protected:
    virtual int numInitStages() const override { return NUM_INIT_STAGES; }
    virtual void initialize(int stage) override;
    virtual void handleMessageWhenUp(cMessage *msg) override;
    virtual void finish() override;
    virtual void refreshDisplay() const override;

    // chooses random destination address
    virtual L3Address chooseDestAddr();
    virtual void sendQoSDataPacket();
    virtual void processPacket(cPacket *msg);
    virtual void setSocketOptions();

    virtual void verifyStatusMsg(cMessage *msg);
    virtual string getNodeName();

    virtual void processStart();
    virtual void processSend();
    virtual void processStop();

    virtual bool handleNodeStart(IDoneCallback *doneCallback) override;
    virtual bool handleNodeShutdown(IDoneCallback *doneCallback) override;
    virtual void handleNodeCrash() override;

    /* Positions Data Methods */
    virtual L3Address controllerAddress(const char *addr);
    virtual void sendPositionData();

  public:
    ApplicationUDP() {}
    ~ApplicationUDP();
};

} // namespace inet

#endif

