#include "inet/networklayer/common/L3AddressResolver.h"
#include "LCPositionsApp.h"
#include "messages/ApplicationMsg_m.h"
#include "messages/StatusMsg_m.h"
#include "inet/common/ModuleAccess.h"
#include "inet/transportlayer/contract/udp/UDPControlInfo_m.h"
#include "messages/MessageStructures.h"
#include <unordered_map>
#include "structures/CarInformations.h"

namespace inet {

Define_Module(LCPositionsApp);

simsignal_t LCPositionsApp::rcvdPkSignal = registerSignal("rcvdPk");

LCPositionsApp::~LCPositionsApp()
{
    cancelAndDelete(selfMsg);
}

void LCPositionsApp::initialize(int stage)
{
    ApplicationBase::initialize(stage);

    if (stage == INITSTAGE_LOCAL) {
        numReceived = 0;
        WATCH(numReceived);

        localPort = par("localPort");
        startTime = par("startTime").doubleValue();
        stopTime = par("stopTime").doubleValue();
        printLocalNetwork = par("printLocalNetwork");
        if (stopTime >= SIMTIME_ZERO && stopTime < startTime)
            throw cRuntimeError("Invalid startTime/stopTime parameters");
        selfMsg = new cMessage("UDPSinkTimer");
    }
}

void LCPositionsApp::handleMessageWhenUp(cMessage *msg)
{
    if (msg->isSelfMessage()) {
        ASSERT(msg == selfMsg);
        switch (selfMsg->getKind()) {
            case START:
                processStart();
                break;

            case STOP:
                processStop();
                break;

            default:
                throw cRuntimeError("Invalid kind %d in self message", (int)selfMsg->getKind());
        }
    }
    else if (msg->getKind() == UDP_I_DATA) {
        // process incoming packet
        processArrivedData(msg);
        processPacket(PK(msg));
        //processStatistics();
    }
    else if (msg->getKind() == UDP_I_ERROR) {
        EV_WARN << "Ignoring UDP error report\n";
        delete msg;
    }
    else {
        throw cRuntimeError("Unrecognized message (%s)%s", msg->getClassName(), msg->getName());
    }
}

void LCPositionsApp::processStatistics()
{
    delayCountVector.record(numReceived);
}

void LCPositionsApp::refreshDisplay() const
{
    char buf[50];
    sprintf(buf, "rcvd: %d pks", numReceived);
    getDisplayString().setTagArg("t", 0, buf);
}

void LCPositionsApp::finish()
{
    ApplicationBase::finish();
    EV_INFO << getFullPath() << ": received " << numReceived << " packets\n";

    delayCountStats.recordAs("PositionsCount");
    delayCountVector.isName("DelayHandover");
}

void LCPositionsApp::setSocketOptions()
{
    bool receiveBroadcast = par("receiveBroadcast");
    if (receiveBroadcast)
        socket.setBroadcast(true);

    MulticastGroupList mgl = getModuleFromPar<IInterfaceTable>(par("interfaceTableModule"), this)->collectMulticastGroups();
    socket.joinLocalMulticastGroups(mgl);

    // join multicastGroup
    const char *groupAddr = par("multicastGroup");
    multicastGroup = L3AddressResolver().resolve(groupAddr);
    if (!multicastGroup.isUnspecified()) {
        if (!multicastGroup.isMulticast())
            throw cRuntimeError("Wrong multicastGroup setting: not a multicast address: %s", groupAddr);
        socket.joinMulticastGroup(multicastGroup);
    }
}

void LCPositionsApp::processStart()
{
    socket.setOutputGate(gate("udpOut"));
    socket.bind(localPort);
    setSocketOptions();

    if (stopTime >= SIMTIME_ZERO) {
        selfMsg->setKind(STOP);
        scheduleAt(stopTime, selfMsg);
    }
}

void LCPositionsApp::processStop()
{
    if (!multicastGroup.isUnspecified())
        socket.leaveMulticastGroup(multicastGroup); // FIXME should be done by socket.close()
    socket.close();
}

void LCPositionsApp::processArrivedData(cMessage *msg)
{
    std::string verify (msg->getClassName());

    if(verify.compare("StatusMsg") == 0){
        StatusMsg *status = check_and_cast<StatusMsg *>(msg);
        if(status->getMsgType() == CAR_POSITION_DATA){
            sendNetViewToCentralController(status);
        }
    }
}

void LCPositionsApp::sendNetViewToCentralController(StatusMsg *status)
{
    //statistics
    //delayCountStats.collect(simTime() - status->getSendTime());
    delayCountVector.record(simTime() - status->getSendTime());

    //Visão da rede local
    localNetView(status);


    //Verifica tempo da simulação e envia de 5 em 5 segundos
    if(simTime().dbl() - time >= 3.0){
        int svrPort = par("centralControllerPort");
        const char *address = par("centralControllerAddress");
        L3Address svrAddr = L3AddressResolver().resolve(address);

        if (svrAddr.isUnspecified()) {
            EV_ERROR << "Central Controller address is unspecified\n";
            return;
        }

        //time = 0.0;
        time = simTime().dbl();
        socket.sendTo(getLocalNetView(), svrAddr, svrPort);
    }



    //lastSend = simTime();

}

void LCPositionsApp::verifyOldDataInNetView()
{
    unordered_map<int,CarInformations>::iterator it;

    for (it = NetView.begin(); it != NetView.end(); it++) {
        int id = it->first;
        CarInformations value = NetView[id];
        int time = value.timeAdded;
        int actualSimTime = getIntTimeSimulation();

        if(actualSimTime - time >= 20){ //Se o tempo de modificação da tabela for maior que 20s deleta da visão da rede
            NetView.erase(id);
        }

    }
}

void LCPositionsApp::localNetView(StatusMsg *status)
{
    //verifyOldDataInNetView();

    CarInformations p;
    string namecar = status->getNodeName();

    insertKeysInMap(getIntTimeSimulation(), namecar, keys);
    p = getPositionFromLine(idNetCar, status);
    insertPositionInMap(idNetCar , p , NetView);
}

int LCPositionsApp::getIntTimeSimulation()
{
    simtime_t time = simTime();

    string idString = time.str().c_str();
    stringstream geek(idString);
    int id = 0;
    geek >> id;

    return id;

}

CarInformations LCPositionsApp::getPositionFromLine(int id, StatusMsg *status)
{
    CarInformations p;
    p.idCar = id;
    p.nameCar = status->getNodeName();
    p.udpPortConn = status->getSourcePort();
    p.addressCar = status->getSourceAddress();
    p.localControllerAddressChoice = status->getDestAddress();
    p.localControllerPortChoice = status->getDestPort();
    p.timeAdded = getIntTimeSimulation();
    p.x = status->getPosX();
    p.y = status->getPosY();
    p.z = status->getPosZ();
    return p;
}

void LCPositionsApp::insertKeysInMap(int id, string namecar, unordered_map< string, int> &someMap)
{
    unordered_map<string ,int>::const_iterator search = someMap.find(namecar);

    //If not have key, the choose key is id parameter
    if ( search == someMap.end() ){
        int selectedId = id++;
        someMap.emplace(namecar, selectedId);
        idNetCar = selectedId;
    }
    else{ //If have key, set this to the specific variable
        idNetCar = search->second;
    }

    //printHashMap(keys);
}

void LCPositionsApp::printHashMap(unordered_map< string, int > someHashMap)
{
    printf("************ Keys VIEW *************\n");
    for (auto& kv : someHashMap) {
        string id = kv.first;
        std::cout << id << "\t-\t";
        std::cout << kv.second << std::endl;
    }
    printf("**********************************************\n");
}

void LCPositionsApp::insertPositionInMap(int id, CarInformations p, unordered_map< int, CarInformations > &someMap)
{
    auto result = someMap.emplace(id, p);
    if (!result.second){
       someMap[id] = p;
    }

    if(printLocalNetwork == true){
        printHashMap(NetView);
    }
}


void LCPositionsApp::printHashMap(unordered_map< int, CarInformations > someHashMap)
{
    cModule *host = getContainingNode(this);
    std::string lcName = host->getFullName();

    printf("************ LOCAL NETWORK VIEW *************\n");
    std::cout << "LOCAL CONTROLLER: " << lcName << std::endl;
    for (auto& kv : someHashMap) {
        int id = kv.first;
        printf("Key - %i\t-\t", id);
        CarInformations value = someHashMap[id];
        string strValue = value.printInformations();
        printf("%s\n", strValue.c_str());

        //Check if have old data Positions
        //if(simTime() - value.timeAdded < 5.0){ //5 seconds
        //    someHashMap.erase(id); //erasing old position
        //}
    }
    printf("**********************************************\n");
}

StatusMsg* LCPositionsApp::getLocalNetView()
{
    std::ostringstream str;
    cModule *host = getContainingNode(this);
    str << "NetView-" << host->getFullName();

    StatusMsg *view = new StatusMsg(str.str().c_str());
    view->setMsgType(LC_LOCALNETVIEW_DATA);
    view->setNodeName(host->getFullName());
    LCnetworkView structView;
    structView.netView = NetView;

    view->setNView(structView);

    return view;

}

void LCPositionsApp::processPacket(cPacket *pk)
{
    EV_INFO << "Received packet: " << UDPSocket::getReceivedPacketInfo(pk) << endl;
    emit(rcvdPkSignal, pk);
    delete pk;

    numReceived++;
}

bool LCPositionsApp::handleNodeStart(IDoneCallback *doneCallback)
{
    simtime_t start = std::max(startTime, simTime());
    if ((stopTime < SIMTIME_ZERO) || (start < stopTime) || (start == stopTime && startTime == stopTime)) {
        selfMsg->setKind(START);
        scheduleAt(start, selfMsg);
    }
    return true;
}

bool LCPositionsApp::handleNodeShutdown(IDoneCallback *doneCallback)
{
    if (selfMsg)
        cancelEvent(selfMsg);
    //TODO if(socket.isOpened()) socket.close();
    return true;
}

void LCPositionsApp::handleNodeCrash()
{
    if (selfMsg)
        cancelEvent(selfMsg);
}

}

