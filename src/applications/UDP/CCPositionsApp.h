#ifndef __CCPositionsApp_H
#define __CCPositionsApp_H

#include "inet/common/INETDefs.h"
#include "inet/common/INETMath.h"
#include <string.h>
#include <omnetpp.h>
#include "inet/applications/base/ApplicationBase.h"
#include "inet/transportlayer/contract/udp/UDPSocket.h"
#include "messages/StatusMsg_m.h"
#include "structures/CarInformations.h"
#include <unordered_map>
#include "structures/LCinformations.h"

namespace inet {

class CCPositionsApp : public ApplicationBase
{
  protected:
    enum SelfMsgKinds { START = 1, STOP };

    UDPSocket socket;
    int localPort = -1;
    L3Address multicastGroup;
    simtime_t startTime;
    simtime_t stopTime;
    simtime_t lastSend;
    cMessage *selfMsg = nullptr;
    cLongHistogram delayCountStats;
    cOutVector overheadCountVector;

    string lessLCAddress;
    int lessLCPort;
    double lessDistance = 0.0;

    string nodeNameTeste;
    string initialLC;
    string actualLC;

    int idNetCar;
    int countKeys;

    bool makeLocalControllerHandover;

    unordered_map< int, CarInformations> GlobalNetView; //LocalView
    unordered_map< int, CarInformations> tempHashMap;
    unordered_map<string, int> keys;
    unordered_map< int, LCinformations> lc_positions;

    int numReceived = 0;
    int numControlMsgReceived;
    static simsignal_t rcvdPkSignal;

    bool printGlobalView;
    double distance;

  public:
    CCPositionsApp() {}
    virtual ~CCPositionsApp();

  protected:
    virtual void processPacket(cPacket *msg);
    virtual void processArrivedData(cMessage *msg);
    virtual void setSocketOptions();

    virtual CarInformations getPositionFromLine(int id, StatusMsg *status);
    virtual void insertPositionInMap(int id, CarInformations p, unordered_map< int, CarInformations > &someMap);
    virtual void insertPositionInMap(int id, LCinformations p, unordered_map< int, LCinformations > &someMap );
    virtual void insertKeysInMap(int id, string namecar, unordered_map< string, int> &someMap);
    virtual void printHashMap(unordered_map< int, CarInformations > someHashMap);
    virtual void printHashMap(unordered_map< string, int > someHashMap);
    virtual void printHashMap(unordered_map< int, LCinformations > someHashMap);
    virtual int getIntTimeSimulation();
    virtual void processStatistics();
    virtual void mergeDataHashMaps(LCnetworkView structView);
    virtual void checkCarPositions();
    virtual void calculateDistance(double carPosX, double carPosY, string addressCar, int portCar);

  protected:
    virtual int numInitStages() const override { return NUM_INIT_STAGES; }
    virtual void initialize(int stage) override;
    virtual void handleMessageWhenUp(cMessage *msg) override;
    virtual void finish() override;
    virtual void refreshDisplay() const override;

    virtual void processStart();
    virtual void processStop();

    virtual bool handleNodeStart(IDoneCallback *doneCallback) override;
    virtual bool handleNodeShutdown(IDoneCallback *doneCallback) override;
    virtual void handleNodeCrash() override;
};

}

#endif

