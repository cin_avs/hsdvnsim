//
// Copyright (C) 2000 Institut fuer Telematik, Universitaet Karlsruhe
// Copyright (C) 2004,2011 Andras Varga
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program; if not, see <http://www.gnu.org/licenses/>.
//

#include "ApplicationUDP.h"

#include "inet/applications/base/ApplicationPacket_m.h"
#include "inet/networklayer/common/L3AddressResolver.h"
#include "inet/common/ModuleAccess.h"
#include "inet/common/lifecycle/NodeOperations.h"
#include "inet/transportlayer/contract/udp/UDPControlInfo_m.h"
#include "messages/StatusMsg_m.h"
#include "messages/ApplicationMsg_m.h"

namespace inet {

Define_Module(ApplicationUDP);

simsignal_t ApplicationUDP::sentPkSignal = registerSignal("sentPk");
simsignal_t ApplicationUDP::rcvdPkSignal = registerSignal("rcvdPk");


ApplicationUDP::~ApplicationUDP()
{
    cancelAndDelete(selfMsg);
}

void ApplicationUDP::initialize(int stage)
{
    ApplicationBase::initialize(stage);

    if (stage == INITSTAGE_LOCAL) {
        numSent = 0;
        numReceived = 0;
        WATCH(numSent);
        WATCH(numReceived);

        localPort = par("localPort");
        destPort = par("destPort");
        startTime = par("startTime").doubleValue();
        stopTime = par("stopTime").doubleValue();
        packetName = par("packetName");
        positionsPacketName = par("positionsPacketName");
        if (stopTime >= SIMTIME_ZERO && stopTime < startTime)
            throw cRuntimeError("Invalid startTime/stopTime parameters");
        selfMsg = new cMessage("sendTimer");
    }
}

void ApplicationUDP::finish()
{
    //recordScalar("packets sent", numSent);
    //recordScalar("packets received", numReceived);
    ApplicationBase::finish();
}



/*
 *********** POSITIONS METHODS TO NETWORK VIEW ***********
 */

L3Address ApplicationUDP::controllerAddress(const char *addr)
{
    const char *connectAddress = addr;
    L3Address destination;
    L3AddressResolver().tryResolve(addr, destination);

    if (destination.isUnspecified()) {
        cout << "Resolve address to " << connectAddress << ": cannot resolve destination address\n";
        EV_INFO << "Resolve address to " << connectAddress << ": cannot resolve destination address\n";
    }

    cout << "Resolve address to " << connectAddress << endl;
    EV_INFO << "Resolve address to " << connectAddress << endl;

    return destination;

}

string ApplicationUDP::getNodeName()
{
    cModule *host = getContainingNode(this);
    string name = host->getFullName();
    return name.c_str();
}

//Send position data to local controller
void ApplicationUDP::sendPositionData()
{
    std::ostringstream str;
    str << positionsPacketName << "-" << numSent;

    cModule *host = getContainingNode(this);
    IMobility *mod = check_and_cast<IMobility *>(host->getSubmodule("mobility"));
    Coord pos = mod->getCurrentPosition();

    L3Address localAddr = L3AddressResolver().resolve(host->getFullName());

    double posX = pos.x;
    double posY = pos.y;
    double posZ = pos.z;

    StatusMsg *msg =  new StatusMsg(str.str().c_str());
    msg->setMsgType(CAR_POSITION_DATA);
    msg->setByteLength(par("messageLength").longValue());
    msg->setNodeName(host->getFullName());
    msg->setSourceAddress(localAddr.str().c_str());
    msg->setSourcePort(localPort);
    msg->setDestAddress(cAddr.str().c_str());
    msg->setDestPort(localPortConn);
    msg->setPosX(posX);
    msg->setPosY(posY);
    msg->setPosZ(posZ);
    msg->setSendTime(simTime());

    emit(sentPkSignal, msg);

    bool verify = par("sendAllToCentralController");

    if(verify){
        //Send to Central Controller
        const char *destAddrs = centralAddrConn;
        cStringTokenizer tokenizer(destAddrs);
        const char *token = nullptr;
        token = tokenizer.nextToken();
        L3Address resolved;
        resolved = L3AddressResolver().resolve(token);
        socket.sendTo(msg, resolved, centralPortConn);
        numSent++;
    }else{
        socket.sendTo(msg, cAddr, localPortConn);
        numSent++;
    }
}


//*************************************

void ApplicationUDP::setSocketOptions()
{
    int timeToLive = par("timeToLive");
    if (timeToLive != -1)
        socket.setTimeToLive(timeToLive);

    int typeOfService = par("typeOfService");
    if (typeOfService != -1)
        socket.setTypeOfService(typeOfService);

    const char *multicastInterface = par("multicastInterface");
    if (multicastInterface[0]) {
        IInterfaceTable *ift = getModuleFromPar<IInterfaceTable>(par("interfaceTableModule"), this);
        InterfaceEntry *ie = ift->getInterfaceByName(multicastInterface);
        if (!ie)
            throw cRuntimeError("Wrong multicastInterface setting: no interface named \"%s\"", multicastInterface);
        socket.setMulticastOutputInterface(ie->getInterfaceId());
    }

    bool receiveBroadcast = par("receiveBroadcast");
    if (receiveBroadcast)
        socket.setBroadcast(true);

    bool joinLocalMulticastGroups = par("joinLocalMulticastGroups");
    if (joinLocalMulticastGroups) {
        MulticastGroupList mgl = getModuleFromPar<IInterfaceTable>(par("interfaceTableModule"), this)->collectMulticastGroups();
        socket.joinLocalMulticastGroups(mgl);
    }
}

L3Address ApplicationUDP::chooseDestAddr()
{
    int k = intrand(destAddresses.size());
    if (destAddresses[k].isLinkLocal()) {    // KLUDGE for IPv6
        const char *destAddrs = par("destAddresses");
        cStringTokenizer tokenizer(destAddrs);
        const char *token = nullptr;

        for (int i = 0; i <= k; ++i)
            token = tokenizer.nextToken();
        destAddresses[k] = L3AddressResolver().resolve(token);
    }
    return destAddresses[k];
}

void ApplicationUDP::sendQoSDataPacket()
{
    cModule *host = getContainingNode(this);
    L3Address localAddr = L3AddressResolver().resolve(host->getFullName());

    string nameApp = packetName;

    if(nameApp.compare("VIDEO") == 0 && msgType == QOS_VIDEO && serverAppPort != 0 && rApp == true){
        std::ostringstream str;
        str << "Request-" << packetName << "-" << numSent;
        StatusMsg *requestApp = new StatusMsg("SETQOSPARAM");
        requestApp->setNodeName(host->getFullName());
        requestApp->setSourceAddress(localAddr.str().c_str());
        requestApp->setSourcePort(localPort);
        requestApp->setQosAction(qos);
        requestApp->setByteLength(par("messageLength").longValue());
        emit(sentPkSignal, requestApp);
        numSent++;
        rApp = false;
        socket.sendTo(requestApp, serverAppAddress, serverAppPort);
    }
    else if((nameApp.compare("FTP") == 0 || nameApp.compare("WWW") == 0) && msgType == QOS_APP && serverAppPort != 0){
        ApplicationMsg *msg =  new ApplicationMsg("AppFrame");
        msg->setByteLength(par("messageLength").longValue());
        msg->setSequenceNumber(numSent);
        msg->setQosParameter(qos);
        msg->setSendTime(simTime());
        emit(sentPkSignal, msg);
        socket.sendTo(msg, serverAppAddress, serverAppPort);
        numSent++;
    }else if(nameApp.compare("NONE") == 0){
        //TODO Do nothing: Ex: NONE
    }
}

void ApplicationUDP::processStart()
{
    socket.setOutputGate(gate("udpOut"));
    const char *localAddress = par("localAddress");
    socket.bind(*localAddress ? L3AddressResolver().resolve(localAddress) : L3Address(), localPort);
    setSocketOptions();

    const char *destAddrs = par("destAddresses");
    cStringTokenizer tokenizer(destAddrs);
    const char *token;

    while ((token = tokenizer.nextToken()) != nullptr) {
        L3Address result;
        L3AddressResolver().tryResolve(token, result);
        if (result.isUnspecified())
            std::cout << "cannot resolve destination address: " << token << std::endl;
        else
            destAddresses.push_back(result);
    }

    if (!destAddresses.empty()) {
        selfMsg->setKind(SEND);
        processSend();
    }
    else {
        if (stopTime >= SIMTIME_ZERO) {
            selfMsg->setKind(STOP);
            scheduleAt(stopTime, selfMsg);
        }
    }
}

void ApplicationUDP::processSend()
{
    if(configuredAddress == true){
        sendQoSDataPacket();
        sendPositionData();
    }

    simtime_t d = simTime() + par("sendInterval").doubleValue();
    if (stopTime < SIMTIME_ZERO || d < stopTime) {
        selfMsg->setKind(SEND);
        scheduleAt(d, selfMsg);
    }
    else {
        selfMsg->setKind(STOP);
        scheduleAt(stopTime, selfMsg);
    }
}

void ApplicationUDP::processStop()
{
    socket.close();
}

void ApplicationUDP::verifyStatusMsg(cMessage *msg)
{
    configuredAddress = true;

    StatusMsg *status = check_and_cast<StatusMsg *>(msg);

    bool decideController = par("sendAllToCentralController");
    int verify = status->getMsgType();

    if(verify == QOS_APP){
        L3AddressResolver().tryResolve(status->getDestAddress(), serverAppAddress);
        serverAppPort = status->getDestPort();
        qos = status->getQosAction();
        msgType = QOS_APP;
    }
    else if(verify == QOS_VIDEO){
        L3AddressResolver().tryResolve(status->getDestAddress(), serverAppAddress);
        serverAppPort = status->getDestPort();
        qos = status->getQosAction();
        msgType = QOS_VIDEO;
    }
    else if(verify == CC_LOCALCONTROLLER_CHANGE){
        //change local controller infos
        localPortConn = status->getDestPortTCP(); //change port of local controller
        L3AddressResolver().tryResolve(status->getDestAddress(), cAddr); //change address of local controller

        send(msg, "upperAppOut"); //send modification LC to AppControl Layer
    }
    else if(decideController == false && verify == LC_ADDRESS_DATA){
        localPortConn = status->getDestPort();
        L3AddressResolver().tryResolve(status->getDestAddress(), cAddr);

    }
    else if(decideController == true){
        centralAddrConn = par("centralControllerAddress");
        centralPortConn = par("centralControllerPort");
    }
}

void ApplicationUDP::handleMessageWhenUp(cMessage *msg)
{
    std::string verify (msg->getClassName());
    if(verify.compare("StatusMsg") == 0){
        verifyStatusMsg(msg);
    }else if(verify.compare("VideoRTPMessage") == 0){
        send(msg, "upperAppOut");
    }

    if (msg->isSelfMessage()) {
        ASSERT(msg == selfMsg);
        switch (selfMsg->getKind()) {
            case START:
                processStart();
                break;

            case SEND:
                processSend();
                break;

            case STOP:
                processStop();
                break;

            default:
                throw cRuntimeError("Invalid kind %d in self message", (int)selfMsg->getKind());
        }
    }
    else if (msg->getKind() == UDP_I_DATA) {
        // process incoming packet
        //processPacket(PK(msg));

    }
    else if (msg->getKind() == UDP_I_ERROR) {
        EV_WARN << "Ignoring UDP error report\n";
        delete msg;
    }
    else {
        throw cRuntimeError("Unrecognized message (%s)%s", msg->getClassName(), msg->getName());
    }
}

void ApplicationUDP::refreshDisplay() const
{
    char buf[100];
    sprintf(buf, "rcvd: %d pks\nsent: %d pks", numReceived, numSent);
    getDisplayString().setTagArg("t", 0, buf);
}

void ApplicationUDP::processPacket(cPacket *pk)
{
    emit(rcvdPkSignal, pk);
    //EV_INFO << "Received packet: " << UDPSocket::getReceivedPacketInfo(pk) << endl;
    delete pk;
    numReceived++;
}

bool ApplicationUDP::handleNodeStart(IDoneCallback *doneCallback)
{
    simtime_t start = std::max(startTime, simTime());
    if ((stopTime < SIMTIME_ZERO) || (start < stopTime) || (start == stopTime && startTime == stopTime)) {
        selfMsg->setKind(START);
        scheduleAt(start, selfMsg);
    }
    return true;
}

bool ApplicationUDP::handleNodeShutdown(IDoneCallback *doneCallback)
{
    if (selfMsg)
        cancelEvent(selfMsg);
    //TODO if(socket.isOpened()) socket.close();
    return true;
}

void ApplicationUDP::handleNodeCrash()
{
    if (selfMsg)
        cancelEvent(selfMsg);
}

} // namespace inet

