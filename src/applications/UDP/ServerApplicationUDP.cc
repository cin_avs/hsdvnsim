#include "inet/networklayer/common/L3AddressResolver.h"
#include "ServerApplicationUDP.h"
#include "messages/ApplicationMsg_m.h"

#include "inet/common/ModuleAccess.h"
#include "inet/transportlayer/contract/udp/UDPControlInfo_m.h"

namespace inet {

Define_Module(ServerApplicationUDP);

simsignal_t ServerApplicationUDP::rcvdPkSignal = registerSignal("rcvdPk");

ServerApplicationUDP::~ServerApplicationUDP()
{
    cancelAndDelete(selfMsg);
}

void ServerApplicationUDP::initialize(int stage)
{
    ApplicationBase::initialize(stage);

    if (stage == INITSTAGE_LOCAL) {
        numReceived = 0;
        WATCH(numReceived);

        localPort = par("localPort");
        startTime = par("startTime").doubleValue();
        stopTime = par("stopTime").doubleValue();
        if (stopTime >= SIMTIME_ZERO && stopTime < startTime)
            throw cRuntimeError("Invalid startTime/stopTime parameters");
        selfMsg = new cMessage("UDPSinkTimer");
    }
}

void ServerApplicationUDP::handleMessageWhenUp(cMessage *msg)
{
    if (msg->isSelfMessage()) {
        ASSERT(msg == selfMsg);
        switch (selfMsg->getKind()) {
            case START:
                processStart();
                break;

            case STOP:
                processStop();
                break;

            default:
                throw cRuntimeError("Invalid kind %d in self message", (int)selfMsg->getKind());
        }
    }
    else if (msg->getKind() == UDP_I_DATA) {
        // process incoming packet
        processStatistics(msg);
        processPacket(PK(msg));

    }
    else if (msg->getKind() == UDP_I_ERROR) {
        EV_WARN << "Ignoring UDP error report\n";
        delete msg;
    }
    else {
        throw cRuntimeError("Unrecognized message (%s)%s", msg->getClassName(), msg->getName());
    }
}

void ServerApplicationUDP::refreshDisplay() const
{
    char buf[50];
    sprintf(buf, "rcvd: %d pks", numReceived);
    getDisplayString().setTagArg("t", 0, buf);
}

void ServerApplicationUDP::finish()
{
    EV << "Server App Received Packets: " << numReceived << endl;
    EV << "Server App Delay count, min:    " << delayCountStats.getMin() << endl;
    EV << "Server App Delay count, max:    " << delayCountStats.getMax() << endl;
    EV << "Server App Delay count, mean:   " << delayCountStats.getMean() << endl;
    EV << "Server App Delay count, stddev: " << delayCountStats.getStddev() << endl;

    ApplicationBase::finish();
    EV_INFO << getFullPath() << ": received " << numReceived << " packets\n";

    //delayCountStats.recordAs("DELAY_ServerAppCount");
}

void ServerApplicationUDP::setSocketOptions()
{
    bool receiveBroadcast = par("receiveBroadcast");
    if (receiveBroadcast)
        socket.setBroadcast(true);

    MulticastGroupList mgl = getModuleFromPar<IInterfaceTable>(par("interfaceTableModule"), this)->collectMulticastGroups();
    socket.joinLocalMulticastGroups(mgl);

    // join multicastGroup
    const char *groupAddr = par("multicastGroup");
    multicastGroup = L3AddressResolver().resolve(groupAddr);
    if (!multicastGroup.isUnspecified()) {
        if (!multicastGroup.isMulticast())
            throw cRuntimeError("Wrong multicastGroup setting: not a multicast address: %s", groupAddr);
        socket.joinMulticastGroup(multicastGroup);
    }
}

void ServerApplicationUDP::processStart()
{
    socket.setOutputGate(gate("udpOut"));
    socket.bind(localPort);
    setSocketOptions();

    if (stopTime >= SIMTIME_ZERO) {
        selfMsg->setKind(STOP);
        scheduleAt(stopTime, selfMsg);
    }
}

void ServerApplicationUDP::processStop()
{
    if (!multicastGroup.isUnspecified())
        socket.leaveMulticastGroup(multicastGroup); // FIXME should be done by socket.close()
    socket.close();
}

void ServerApplicationUDP::processStatistics(cMessage *msg)
{
    std::string verify = msg->getFullName();

    if(verify.compare("AppFrame") == 0){
        ApplicationMsg *pkt = check_and_cast<ApplicationMsg *>(msg);
        simtime_t delay = simTime() - pkt->getSendTime();
        delayCountVector.record(delay);
        delayCountStats.collect(delay);
    }
}

void ServerApplicationUDP::processPacket(cPacket *pk)
{
    EV_INFO << "Received packet: " << UDPSocket::getReceivedPacketInfo(pk) << endl;
    emit(rcvdPkSignal, pk);
    delete pk;

    numReceived++;
}

bool ServerApplicationUDP::handleNodeStart(IDoneCallback *doneCallback)
{
    simtime_t start = std::max(startTime, simTime());
    if ((stopTime < SIMTIME_ZERO) || (start < stopTime) || (start == stopTime && startTime == stopTime)) {
        selfMsg->setKind(START);
        scheduleAt(start, selfMsg);
    }
    return true;
}

bool ServerApplicationUDP::handleNodeShutdown(IDoneCallback *doneCallback)
{
    if (selfMsg)
        cancelEvent(selfMsg);
    //TODO if(socket.isOpened()) socket.close();
    return true;
}

void ServerApplicationUDP::handleNodeCrash()
{
    if (selfMsg)
        cancelEvent(selfMsg);
}

}

