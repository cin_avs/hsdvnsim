#ifndef __LCPositionsApp_H
#define __LCPositionsApp_H

#include "inet/common/INETDefs.h"
#include <string.h>
#include <omnetpp.h>
#include "inet/applications/base/ApplicationBase.h"
#include "inet/transportlayer/contract/udp/UDPSocket.h"
#include "messages/StatusMsg_m.h"
#include "structures/CarInformations.h"
#include <unordered_map>

namespace inet {

/**
 * Consumes and prints packets received from the UDP module.
 *
 */
class LCPositionsApp : public ApplicationBase
{
  protected:
    enum SelfMsgKinds { START = 1, STOP };

    UDPSocket socket;
    int localPort = -1;
    L3Address multicastGroup;
    simtime_t startTime;
    simtime_t stopTime;
    simtime_t lastSend;

    double time;

    cMessage *selfMsg = nullptr;
    cLongHistogram delayCountStats;
    cOutVector delayCountVector;
    bool printLocalNetwork;

    int idNetCar;
    int countKeys;

    unordered_map< int, CarInformations> NetView; //LocalView
    unordered_map<string, int> keys;

    int numReceived = 0;
    static simsignal_t rcvdPkSignal;

  public:
    LCPositionsApp() {}
    virtual ~LCPositionsApp();

  protected:
    virtual void processPacket(cPacket *msg);
    virtual void processArrivedData(cMessage *msg);
    virtual void sendNetViewToCentralController(StatusMsg *status);
    virtual void setSocketOptions();
    virtual void localNetView(StatusMsg *status);
    virtual StatusMsg* getLocalNetView();
    virtual CarInformations getPositionFromLine(int id, StatusMsg *status);
    virtual void insertPositionInMap(int id, CarInformations p, unordered_map< int, CarInformations > &someMap);
    virtual void insertKeysInMap(int id, string namecar, unordered_map< string, int> &someMap);
    virtual void printHashMap(unordered_map< int, CarInformations > someHashMap);
    virtual void printHashMap(unordered_map< string, int > someHashMap);
    virtual int getIntTimeSimulation();
    virtual void verifyOldDataInNetView();
    virtual void processStatistics();

  protected:
    virtual int numInitStages() const override { return NUM_INIT_STAGES; }
    virtual void initialize(int stage) override;
    virtual void handleMessageWhenUp(cMessage *msg) override;
    virtual void finish() override;
    virtual void refreshDisplay() const override;

    virtual void processStart();
    virtual void processStop();

    virtual bool handleNodeStart(IDoneCallback *doneCallback) override;
    virtual bool handleNodeShutdown(IDoneCallback *doneCallback) override;
    virtual void handleNodeCrash() override;
};

}

#endif

