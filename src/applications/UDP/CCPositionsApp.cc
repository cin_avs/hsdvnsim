#include "inet/networklayer/common/L3AddressResolver.h"
#include "CCPositionsApp.h"
#include "messages/ApplicationMsg_m.h"
#include "messages/StatusMsg_m.h"
#include "inet/common/ModuleAccess.h"
#include "inet/transportlayer/contract/udp/UDPControlInfo_m.h"
#include "messages/MessageStructures.h"
#include <unordered_map>

namespace inet {

Define_Module(CCPositionsApp);

simsignal_t CCPositionsApp::rcvdPkSignal = registerSignal("rcvdPk");

CCPositionsApp::~CCPositionsApp()
{
    cancelAndDelete(selfMsg);
}

void CCPositionsApp::initialize(int stage)
{
    ApplicationBase::initialize(stage);

    if (stage == INITSTAGE_LOCAL) {
        numReceived = 0;
        WATCH(numReceived);

        localPort = par("localPort");
        startTime = par("startTime").doubleValue();
        stopTime = par("stopTime").doubleValue();
        printGlobalView = par("printGlobalView");
        makeLocalControllerHandover = par("makeLocalControllerHandover");
        if (stopTime >= SIMTIME_ZERO && stopTime < startTime)
            throw cRuntimeError("Invalid startTime/stopTime parameters");
        selfMsg = new cMessage("UDPSinkTimer");
    }
}

void CCPositionsApp::handleMessageWhenUp(cMessage *msg)
{
    if (msg->isSelfMessage()) {
        ASSERT(msg == selfMsg);
        switch (selfMsg->getKind()) {
            case START:
                processStart();
                break;

            case STOP:
                processStop();
                break;

            default:
                throw cRuntimeError("Invalid kind %d in self message", (int)selfMsg->getKind());
        }
    }
    else if (msg->getKind() == UDP_I_DATA) {
        // process incoming packet
        processArrivedData(msg);
        processPacket(PK(msg));
        processStatistics();
        checkCarPositions();
    }
    else if (msg->getKind() == UDP_I_ERROR) {
        EV_WARN << "Ignoring UDP error report\n";
        delete msg;
    }
    else {
        throw cRuntimeError("Unrecognized message (%s)%s", msg->getClassName(), msg->getName());
    }
}

void CCPositionsApp::processStatistics()
{
    overheadCountVector.record(numControlMsgReceived);
    delayCountStats.collect(numReceived);
}

void CCPositionsApp::refreshDisplay() const
{
    char buf[50];
    sprintf(buf, "rcvd: %d pks", numReceived);
    getDisplayString().setTagArg("t", 0, buf);
}

void CCPositionsApp::finish()
{
    ApplicationBase::finish();
    EV_INFO << getFullPath() << ": received " << numReceived << " packets\n";

    delayCountStats.recordAs("DelayCount");
}

void CCPositionsApp::setSocketOptions()
{
    bool receiveBroadcast = par("receiveBroadcast");
    if (receiveBroadcast)
        socket.setBroadcast(true);

    MulticastGroupList mgl = getModuleFromPar<IInterfaceTable>(par("interfaceTableModule"), this)->collectMulticastGroups();
    socket.joinLocalMulticastGroups(mgl);

    // join multicastGroup
    const char *groupAddr = par("multicastGroup");
    multicastGroup = L3AddressResolver().resolve(groupAddr);
    if (!multicastGroup.isUnspecified()) {
        if (!multicastGroup.isMulticast())
            throw cRuntimeError("Wrong multicastGroup setting: not a multicast address: %s", groupAddr);
        socket.joinMulticastGroup(multicastGroup);
    }
}

void CCPositionsApp::processStart()
{
    socket.setOutputGate(gate("udpOut"));
    socket.bind(localPort);
    setSocketOptions();

    if (stopTime >= SIMTIME_ZERO) {
        selfMsg->setKind(STOP);
        scheduleAt(stopTime, selfMsg);
    }
}

void CCPositionsApp::processStop()
{
    if (!multicastGroup.isUnspecified())
        socket.leaveMulticastGroup(multicastGroup); // FIXME should be done by socket.close()
    socket.close();
}

void CCPositionsApp::processArrivedData(cMessage *msg)
{
    std::string verify (msg->getClassName());

    if(verify.compare("StatusMsg") == 0){
        StatusMsg *status = check_and_cast<StatusMsg *>(msg);
        if(status->getMsgType() == LC_LOCALNETVIEW_DATA){
            LCnetworkView structView;
            structView = status->getNView();
            mergeDataHashMaps(structView);

            numControlMsgReceived++;
        }
        else if(status->getMsgType() == CAR_POSITION_DATA){
            CarInformations value;
            string namecar = status->getNodeName();

            value.nameCar = status->getNodeName();
            value.addressCar = status->getSourceAddress();
            value.x = status->getPosX();
            value.y = status->getPosY();
            value.z = status->getPosZ();

            insertKeysInMap(getIntTimeSimulation(), namecar, keys);
            insertPositionInMap(idNetCar , value , GlobalNetView);

            numControlMsgReceived++;
        }
        else if(status->getMsgType() == LC_POSITIONS_TO_UDPAPP){
            LCinformations l;
            string id = status->getNodeName();
            char c = id[2]; //getting id from message Ex: lC1 -> 1
            int idValue = c - '0'; //converting char to int
            l.idLC = idValue;
            l.address = status->getSourceAddress();
            l.port = status->getSourcePort();
            l.x = status->getPosX();
            l.y = status->getPosY();
            l.z = status->getPosZ();
            insertPositionInMap(idValue , l , lc_positions);
        }
    }
}

void CCPositionsApp::mergeDataHashMaps(LCnetworkView structView)
{
    tempHashMap = structView.netView;
    CarInformations value;
    string namecar;

    for (auto& kv : tempHashMap) {
        int id = kv.first;
        value = tempHashMap[id];
        namecar = value.nameCar;
        insertKeysInMap(getIntTimeSimulation(), namecar, keys);
        insertPositionInMap(idNetCar , value , GlobalNetView);
    }

}

void CCPositionsApp::checkCarPositions()
{
    for (auto& kv : GlobalNetView) {
        int id = kv.first;
        CarInformations value = GlobalNetView[id];
        initialLC = value.localControllerAddressChoice;
        calculateDistance(value.x, value.y, value.addressCar, value.udpPortConn);
    }

    lessDistance = 0.0;
}

void CCPositionsApp::calculateDistance(double carPosX, double carPosY, string addressCar, int portCar)
{
    double lcPosX;
    double lcPosY;
    LCinformations value;

    for (auto& kv : lc_positions) {
        int id = kv.first;
        value = lc_positions[id];
        lcPosX = value.x;
        lcPosY = value.y;
        distance = sqrt(pow(lcPosX - carPosX,  2) + pow(lcPosY - carPosY , 2));

        if(lessDistance == 0.0 || distance < lessDistance){
            lessLCAddress = value.address;
            lessLCPort = value.port;
            lessDistance = distance;
        }else{
            distance = lessDistance;
        }
    }

    if(makeLocalControllerHandover == true){
     if(initialLC.compare(lessLCAddress) != 0){
         StatusMsg *response = new StatusMsg("LController Changes");
         response->setMsgType(CC_LOCALCONTROLLER_CHANGE);
         response->setDestAddress(lessLCAddress.c_str());
         response->setDestPortTCP(lessLCPort);
         response->setSourceAddress(addressCar.c_str());
         response->setSourcePort(portCar);
         response->setByteLength(100);
         L3Address svrAddr = L3AddressResolver().resolve(addressCar.c_str());
         socket.sendTo(response, svrAddr, portCar);
     }
    }

}

int CCPositionsApp::getIntTimeSimulation()
{
    simtime_t time = simTime();

    string idString = time.str().c_str();
    stringstream geek(idString);
    int id = 0;
    geek >> id;

    return id;

}

CarInformations CCPositionsApp::getPositionFromLine(int id, StatusMsg *status)
{
    CarInformations p;
    p.idCar = id;
    p.nameCar = status->getNodeName();
    p.addressCar = status->getSourceAddress();
    p.timeAdded = getIntTimeSimulation();
    p.x = status->getPosX();
    p.y = status->getPosY();
    p.z = status->getPosZ();
    return p;
}

void CCPositionsApp::insertKeysInMap(int id, string namecar, unordered_map< string, int> &someMap)
{
    unordered_map<string ,int>::const_iterator search = someMap.find(namecar);

    //If not have key, the choose key is id parameter
    if ( search == someMap.end() ){
        int selectedId = id++;
        someMap.emplace(namecar, selectedId);
        idNetCar = selectedId;
    }
    else{ //If have key, set this to the specific variable
        idNetCar = search->second;
    }

    //printHashMap(keys);
}

void CCPositionsApp::printHashMap(unordered_map< string, int > someHashMap)
{
    printf("************ Keys VIEW *************\n");
    for (auto& kv : someHashMap) {
        string id = kv.first;
        std::cout << id << "\t-\t";
        std::cout << kv.second << std::endl;
    }
    printf("**********************************************\n");
}

void CCPositionsApp::printHashMap(unordered_map< int, LCinformations > someHashMap){

    printf("********** CENTRAL CONTROLLER INFOS *************\n");
    for (auto& kv : someHashMap) {
        int id = kv.first;
        printf("Local Controller - %i\t-\t", id);
        LCinformations value = someHashMap[id];
        string strValue = value.printInformations();
        printf("%s\n", strValue.c_str());
    }
    printf("**********************************************\n");
}

void CCPositionsApp::insertPositionInMap(int id, CarInformations p, unordered_map< int, CarInformations > &someMap)
{
    auto result = someMap.emplace(id, p);
    if (!result.second){
       someMap[id] = p;
    }

    if(printGlobalView == true){
        printHashMap(GlobalNetView);
    }

}

void CCPositionsApp::insertPositionInMap(int id, LCinformations p, unordered_map< int, LCinformations > &someMap )
{
    auto result = someMap.emplace(id, p);
    if (!result.second){
        someMap[id] = p;
    }
    printHashMap(lc_positions);
}

void CCPositionsApp::printHashMap(unordered_map< int, CarInformations > someHashMap)
{
    cModule *host = getContainingNode(this);
    std::string ccName = host->getFullName();

    printf("************ CENTRAL NETWORK VIEW *************\n");
    std::cout << "CENTRAL CONTROLLER: " << ccName << std::endl;
    for (auto& kv : someHashMap) {
        int id = kv.first;
        printf("Key - %i\t-\t", id);
        CarInformations value = someHashMap[id];
        string strValue = value.printInformations();
        printf("%s\n", strValue.c_str());
    }
    printf("**********************************************\n");
}

void CCPositionsApp::processPacket(cPacket *pk)
{
    //EV_INFO << "Received packet: " << UDPSocket::getReceivedPacketInfo(pk) << endl;
    emit(rcvdPkSignal, pk);
    delete pk;

    numReceived++;
}

bool CCPositionsApp::handleNodeStart(IDoneCallback *doneCallback)
{
    simtime_t start = std::max(startTime, simTime());
    if ((stopTime < SIMTIME_ZERO) || (start < stopTime) || (start == stopTime && startTime == stopTime)) {
        selfMsg->setKind(START);
        scheduleAt(start, selfMsg);
    }
    return true;
}

bool CCPositionsApp::handleNodeShutdown(IDoneCallback *doneCallback)
{
    if (selfMsg)
        cancelEvent(selfMsg);
    //TODO if(socket.isOpened()) socket.close();
    return true;
}

void CCPositionsApp::handleNodeCrash()
{
    if (selfMsg)
        cancelEvent(selfMsg);
}

}

