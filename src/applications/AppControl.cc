#include "AppControl.h"
#include "messages/StatusMsg_m.h"
#include "messages/OpenFlowMsg_m.h"
#include "messages/MessageStructures.h"
#include <unordered_map>
#include "structures/CarInformations.h"
#include "structures/LCinformations.h"
#include "structures/openflow.h"
#include "inet/common/ModuleAccess.h"
#include "inet/mobility/contract/IMobility.h"
#include "structures/Table.h"
#include "structures/FlowTable.h"

Define_Module(AppControl);

void AppControl::initialize(){/*TODO*/}

void AppControl::handleMessage(cMessage *msg)
{
    std::string verify (msg->getClassName());

    if(verify.compare("StatusMsg") == 0){
        StatusMsg *status = check_and_cast<StatusMsg *>(msg);

        switch(status->getMsgType()){
            case OF_REQUEST_LC_ADDR: //CentralController
                processOFRLA(status);
                break;
            case LC_CHOICE_BY_CAR:   //Cars
                processLCBC(status);
                break;
            case VEHICLE_STATUS_POSITION:  //LocalController
                processVSP(status);
                break;
            case STATUS_QOS_REQUEST: //Cars:
                processFlowTableRequest(status);
                break;
            case STATUS_GET_QOS_REQUEST: //LocalControllers
                //getQoS(status);
            default:
                EV << "Unknown message type: " << status->getName() << "," << status->getMsgType();
                break;
        }
    }
    else if(verify.compare("OpenFlowMsg") == 0){
        OpenFlowMsg *ofm = check_and_cast<OpenFlowMsg *>(msg);
        switch(ofm->getOFtype()){
                //controlador central recebe solicitação de informações do controlador local

            //case OFPT_TABLE_MISS_REPLY:  //controlador central responde para veículo as informações dos controladores locais
            //    break;
            case OFPT_QOS_REQUEST: //---> LocalControllers: "Send priority to Cars"
                processFeaturesRequest(ofm);
                break;
            case OFPT_QOS_REPLY:  //car recebe prioridade
                keepingPriority(ofm);
                break;
            case OFPT_CONTROLLER_STATUS: //Central Controller: "Local Controlers Positions"
                insertLocalControllerStatus(ofm);
                break;
            case OFPT_GET_CONFIG_REQUEST: //Central Controller: "Send Localcontrollers Positions to Cars"
                sendBackLocalControllerInfos(ofm);
                break;
        }
    }
    else{
        EV << "Unknown Message: " << msg->getClassName() << std::endl;
        std::cout << "Unknown Message: " << msg->getClassName() << std::endl;
        delete msg;
    }

}

void AppControl::keepingPriority(OpenFlowMsg *ofm)
{
    //Table t;

    appType = ofm->getAppType();
    serverAddr = ofm->getDestAddress();
    serverPort = ofm->getDestPort();

    ofp_flow_table_header_fields rule;
    rule = ofm->getRule();
    table.addTableEntry(ofm->getAppType(), rule); //Adiciona Tabela com regra de QoS
    table.printTableEntry(); //PRINT DA TABELA ADICIONADA
    flowTable.addFlowEntry(countFT, table);

    //SE APLICAÇÃO FOR VIDEO ENVIAR PARAMETRO DE QOS PARA MODULO RTPINTERFACE
    if(appType == 1){
        StatusMsg *response = new StatusMsg("QOS_VIDEO");
        response->setMsgType(QOS_VIDEO);
        response->setQosAction(rule.action);
        send(response, "rtpInterfaceOut");
    }else{
        StatusMsg *response = new StatusMsg("QOS_APP");
        response->setMsgType(QOS_APP);
        response->setQosAction(rule.action);
        send(response, "udpdataOut");
    }
}

int AppControl::verifyClusters(OpenFlowMsg *ofm)
{
    int appType = ofm->getAppType();
    int priority = OFPT_PRIORITY_NOTHING;

    string nodeName = ofm->getNodeName();

    //if(appType == 1 && ag7.size() < 10){
    //    ag7.push_back(nodeName);
    //    priority = OFPT_PRIORITY_SEVEN;
    //    printAggregationMap();
    //}

    if(appType == 1 && ag6.size() < 10){
        ag6.push_back(nodeName);
        priority = OFPT_PRIORITY_SIX;
        printAggregationMap();
    }
    else if((appType == 1) && ag5.size() < 10){
        ag5.push_back(nodeName);
        priority = OFPT_PRIORITY_FIVE;
        printAggregationMap();

    }

    //else if((appType == 1) && ag4.size() < 10){
    //    ag4.push_back(nodeName);
    //    priority = OFPT_PRIORITY_FOUR;
    //    printAggregationMap();
    //}
    else if((appType == 1) && ag3.size() < 10){
        ag3.push_back(nodeName);
        priority = OFPT_PRIORITY_THREE;
        printAggregationMap();
    }

    else if((appType == 2 || appType == 3) && ag1.size() < 10){
        ag2.push_back(nodeName);
        priority = OFPT_PRIORITY_ONE;
        printAggregationMap();

    }
    else if((appType == 2 || appType == 3) && ag0.size() < 10){
        ag1.push_back(nodeName);
        priority = OFPT_PRIORITY_NOTHING;
        printAggregationMap();
    }
    //else{
    //    ag0.push_back(nodeName);
    //    printAggregationMap();
    //}
    return priority;
}

void AppControl::printAggregationMap()
{
    std::cout << "Aggregation 7" << std::endl;
    for(auto &i : ag7){
        cout << i << ", ";
    }

    cout << "\n";
    std::cout << "Aggregation 6" << std::endl;
    for(auto &i : ag6){
        cout << i << ", ";
    }

    cout << "\n";
    std::cout << "Aggregation 5" << std::endl;
    for(auto &i : ag5){
        cout << i << ", ";
    }

    cout << "\n";
    std::cout << "Aggregation 4" << std::endl;
    for(auto &i : ag4){
        cout << i << ", ";
    }

    cout << "\n";
    std::cout << "Aggregation 3" << std::endl;
    for(auto &i : ag3){
        cout << i << ", ";
    }

    cout << "\n";
    std::cout << "Aggregation 2" << std::endl;
    for(auto &i : ag2){
        cout << i << ", ";
    }

    cout << "\n";
    std::cout << "Aggregation 1" << std::endl;
    for(auto &i : ag1){
        cout << i << ", ";
    }

    cout << "\n";
    std::cout << "Aggregation 0" << std::endl;
    for(auto &i : ag0){
        cout << i << ", ";
    }
}


void AppControl::processFeaturesRequest(OpenFlowMsg *ofm)
{
    int priority = verifyClusters(ofm);

    ofp_flow_table_header_fields rule;
    rule.sourcePort = ofm->getSourcePort();
    rule.srcMAC = "-";
    rule.dstMAC = "-";
    rule.vlanId = 0;
    rule.priority = 100;
    rule.ethernetType = 0;
    rule.srcIP = "-";
    rule.dstIP = "-";
    rule.ipProto = "-";
    rule.srcTcpPort = ofm->getSourcePort();
    rule.action = priority;
    rule.counter = 1;

    OpenFlowMsg *response = new OpenFlowMsg("QoS_Reply");
    response->setOFtype(OFPT_QOS_REPLY);
    response->setRule(rule);
    response->setAppType(ofm->getAppType());
    send(response, "tcpdataOut");
}

void AppControl::createTableMiss(int sourcePort, string sourceAddress)
{
    ofp_flow_table_header_fields match;
    //Primeiramente define uma TableMiss;

    match.sourcePort    = sourcePort;
    match.srcMAC        = "-";
    match.dstMAC        = "-";
    match.vlanId        = 0;
    match.priority      = 1;
    match.ethernetType  = 0;
    match.srcIP         = sourceAddress;
    match.dstIP         = localControllerChoice;
    match.ipProto       = "-";
    match.srcTcpPort    = sourcePort;
    match.dstTcpPort    = localControllerPortChoice;
    match.action        = OFPT_TABLE_MISS;
    match.counter       = count++;

    table.addTableEntry(OF_TABLE_MISS, match); //Adiciona TableMiss
    flowTable.addFlowEntry(countFT, table);
}

void AppControl::processFlowTableRequest(StatusMsg *status)
{
    if(createTM){
        createTableMiss(status->getSourcePort(), status->getSourceAddress());
        createTM = false;
    }

    Table t;
    StatusMsg *response = new StatusMsg("QoS_Rule");
    ofp_flow_table_header_fields rule;
    t = flowTable.getTable(countFT);
    countFT++;
    rule = t.getRule(OF_TABLE_MISS);
    response->setMsgType(STATUS_TABLE_MISS);
    response->setDestAddress(rule.dstIP.c_str());
    response->setDestPortTCP(rule.dstTcpPort);
    send(response, "tcpdataOut"); //TCP envia requisição de QoS
}

void AppControl::testaCodigo(std::string msg)
{
    std::cout << "INFO: " <<  msg << std::endl;
}

void AppControl::insertLocalControllerStatus(OpenFlowMsg *status)
{
    LCinformations l;
    string id = status->getNodeName();
    char c = id[2]; //getting id from message Ex: lC1 -> 1
    int idValue = c - '0'; //converting char to int
    l.idLC = idValue;
    l.address = status->getSourceAddress();
    l.port = status->getSourcePort();
    l.x = status->getPosX();
    l.y = status->getPosY();
    l.z = status->getPosZ();
    insertPositionInMap(idValue , l , lc_positions);
}

void AppControl::sendBackLocalControllerInfos(OpenFlowMsg *status)
{
    OpenFlowMsg *msg = check_and_cast<OpenFlowMsg *>(status);
    OpenFlowMsg *response = new OpenFlowMsg("LocalControllers Informations");

    response->setOFtype(OFPT_GET_CONFIG_REPLY);
    response->setDestAddress(msg->getSourceAddress());
    response->setDestPort(msg->getSourcePort());
    response->setInfo(getLCPositions());

    send(response, "tcpdataOut");
}

void AppControl::processOFRLA(StatusMsg *status)
{

    StatusMsg *response = new StatusMsg("RESPONSE_WITH_LC_ADDRESS_TO_OPENFLOW");
    response->setMsgType(OF_RESPONSE_LC_ADDR);
    response->setDestAddress(localControllerChoice.c_str());
    send(response, "tcpdataOut");

}

void AppControl::processLCBC(StatusMsg *status)
{
    localControllerChoice = status->getDestAddress();
    localControllerPortChoice = status->getDestPortTCP();
}

localControllerInformations AppControl::getLCPositions()
{
    localControllerInformations infos;

    for (auto& kv : lc_positions) {
        int id = kv.first;
        //printf("Local Controller - %i\t-\t", id);
        LCinformations value = lc_positions[id];
        infos.id.push_back(value.getID_LC());
        infos.address.push_back(value.getAddress_LC());
        infos.port.push_back(value.getPort_LC());
        infos.posX.push_back(value.getPOSX_LC());
        infos.posY.push_back(value.getPOSY_LC());
        infos.posZ.push_back(value.getPOSZ_LC());
     }
    return infos;
}


void AppControl::processVSP(StatusMsg *status)
{
    CarInformations p;

    string idcar = status->getNodeName();
    char idcar2 = idcar[4];
    int idcar3 = idcar2 - '0';

    p = getPositionFromLine(idcar3, status);
    insertPositionInMap(idcar3 , p , nV_position);
    printHashMap(nV_position);

}


CarInformations AppControl::getPositionFromLine(int id, StatusMsg *status){
    CarInformations p;
    p.idCar = id;
    p.nameCar = status->getNodeName();
    p.addressCar = status->getSourceAddress();
    p.x = status->getPosX();
    p.y = status->getPosY();
    p.z = status->getPosZ();
    return p;
}

void AppControl::insertPositionInMap(int id, CarInformations p, unordered_map< int, CarInformations > &someMap ){

    std::string verify = p.getNameCar().c_str();
    auto result = someMap.emplace(id, p);
    if (!result.second){
       someMap[id] = p;
    }

    printHashMap(nV_position);
}

void AppControl::printHashMap(unordered_map< int, CarInformations > someHashMap){

    printf("********** Network View Informations\n");
    for (auto& kv : someHashMap) {
        int id = kv.first;
        printf("Car - %i\t-\t", id);
        CarInformations value = someHashMap[id];
        string strValue = value.printInformations();
        printf("%s\n", strValue.c_str());
    }
    printf("**********\n");
}

void AppControl::insertPositionInMap(int id, LCinformations p, unordered_map< int, LCinformations > &someMap ){
    auto result = someMap.emplace(id, p);
    if (!result.second){
        //printf("ID %i ALREADY EXISTS IN POSITIONS. WORKING. \n", id);
        someMap[id] = p;
    }

    printHashMap(lc_positions);
}

void AppControl::printHashMap(unordered_map< int, LCinformations > someHashMap){

    printf("********** Local Controllers Infos\n");
    for (auto& kv : someHashMap) {
        int id = kv.first;
        printf("Local Controller - %i\t-\t", id);
        LCinformations value = someHashMap[id];
        string strValue = value.printInformations();
        printf("%s\n", strValue.c_str());
    }
    printf("**********\n");
}
