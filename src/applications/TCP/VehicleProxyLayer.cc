#include "VehicleProxyLayer.h"

#include "inet/common/RawPacket.h"
#include "inet/transportlayer/contract/tcp/TCPCommand_m.h"
#include "inet/common/ModuleAccess.h"
#include "inet/common/lifecycle/NodeOperations.h"
#include "messages/StatusMsg_m.h"
#include "messages/OpenFlowMsg_m.h"
#include "messages/MessageStructures.h"
#include "inet/networklayer/common/L3AddressResolver.h"
#include "inet/mobility/contract/IMobility.h"

namespace inet {

Define_Module(VehicleProxyLayer);

simsignal_t VehicleProxyLayer::rcvdPkSignal = registerSignal("rcvdPk");
simsignal_t VehicleProxyLayer::sentPkSignal = registerSignal("sentPk");

void VehicleProxyLayer::initialize(int stage)
{
    cSimpleModule::initialize(stage);

    if (stage == INITSTAGE_LOCAL) {
        delay = par("echoDelay");
        echoFactor = par("echoFactor");
        usingQoS = par("usingQoS");
        appTYPE = par("appTYPE");

        bytesRcvd = bytesSent = 0;
        WATCH(bytesRcvd);
        WATCH(bytesSent);

        socket.setOutputGate(gate("tcpOut"));
        socket.readDataTransferModePar(*this);



        nodeStatus = dynamic_cast<NodeStatus *>(findContainingNode(this)->getSubmodule("status"));
    }
    else if (stage == INITSTAGE_APPLICATION_LAYER) {
        if (isNodeUp()){
            startListening();
            verifyPositionLC();
        }
    }
}

bool VehicleProxyLayer::isNodeUp()
{
    return !nodeStatus || nodeStatus->getState() == NodeStatus::UP;
}

void VehicleProxyLayer::startListening()
{
    const char *localAddress = par("localAddress");
    int localPort = par("localPort");
    socket.renewSocket();
    socket.bind(localAddress[0] ? L3Address(localAddress) : L3Address(), localPort);
    socket.listen();
}

void VehicleProxyLayer::processCCRVI(OpenFlowMsg *status)
{
    //Process positions of LCs on the Vehicle
    cModule *host = nullptr;
    host = getContainingNode(this);
    localControllerInformations infos;
    infos = status->getInfo();


    for(int i = 0; i < infos.id.size(); i++){
        calculateDistance(infos.address[i], infos.port[i], infos.posX[i], infos.posY[i]);
    }

    StatusMsg *msg = new StatusMsg("LocalControllerInformations");
    msg->setMsgType(LC_CHOICE_BY_CAR);
    msg->setDestAddress(lCAddrConn.c_str());
    msg->setDestPortTCP(lCPortConn);
    send(msg, "upperTCPAppOut");

    if(usingQoS){ //significa que foi setado para o carro solicitar parametros de qos
        int localPort = par("localPort");
        L3Address localAddr = L3AddressResolver().resolve(host->getFullName());
        StatusMsg *msg = new StatusMsg("VerifyFlowTable");
        msg->setMsgType(STATUS_QOS_REQUEST);
        msg->setSourceAddress(localAddr.str().c_str());
        msg->setSourcePort(localPort);
        send(msg, "upperTCPAppOut");
    }

}

void VehicleProxyLayer::calculateDistance(string lCaddress, int lCPort, double lcPosX, double lcPosY)
{
    cModule *host = nullptr;
    host = getContainingNode(this);
    IMobility  *mod = check_and_cast<IMobility *>(host->getSubmodule("mobility"));
    Coord pos = mod->getCurrentPosition();

    double coord_X = pos.x;
    double coord_Y = pos.y;
    //double coord_Z = pos.z;

    distance = sqrt(pow(lcPosX - coord_X,  2) + pow(lcPosY - coord_Y , 2));

    if(lessDistance == 0.0 || distance < lessDistance){
        lCAddrConn = lCaddress;
        lCPortConn = lCPort;
        lessDistance = distance;
    }else{
        distance = lessDistance;
    }

}


void VehicleProxyLayer::verifyPositionLC()
{
    int contrPort = par("centralControllerPort");
    int localPort = par("localPort");
    const char *address = par("centralControllerAddress");

    connect(address, contrPort);

    cModule *host = nullptr;
    host = getContainingNode(this);

    L3Address localAddr = L3AddressResolver().resolve(host->getFullName());

    OpenFlowMsg *status = new OpenFlowMsg("Veh_Init_Req");
    status->setOFtype(OFPT_GET_CONFIG_REQUEST);
    status->setNodeName(host->getFullName());
    status->setSourceAddress(localAddr.str().c_str());
    status->setSourcePort(localPort);
    status->setByteLength(100);

    sendPacket(status);

}

void VehicleProxyLayer::sendPacket(cPacket *msg)
{
    int numBytes = msg->getByteLength();
    emit(sentPkSignal, msg);
    socket.send(msg);

    //packetsSent++;
    bytesSent += numBytes;
}

void VehicleProxyLayer::processOfData(cMessage *msg)
{
    OpenFlowMsg *status = check_and_cast<OpenFlowMsg *>(msg);
    if(status->getOFtype() == OFPT_GET_CONFIG_REPLY){ //informações do controlador central com as posições dos controladores locais
        processCCRVI(status);
    }
    else if(status->getOFtype() == OFPT_QOS_REPLY){ //Mensagem de QoS do controlador Local
        send(msg, "upperTCPAppOut");
    }
}


void VehicleProxyLayer::processStatusData(cMessage *msg)
{
    StatusMsg *status = check_and_cast<StatusMsg *>(msg);
    if(status->getMsgType() == STATUS_TABLE_MISS){
        processFlowTableRequest(status);
    }
}

void VehicleProxyLayer::processFlowTableRequest(StatusMsg *status)
{
    connect(status->getDestAddress(), status->getDestPortTCP());

    int localPort = par("localPort");
    cModule *host = nullptr;
    host = getContainingNode(this);

    L3Address localAddr = L3AddressResolver().resolve(host->getFullName());

    OpenFlowMsg *request = new OpenFlowMsg("QoS_Request");
    request->setOFtype(OFPT_QOS_REQUEST);
    request->setAppType(appTYPE);
    request->setNodeName(host->getFullName());
    request->setSourceAddress(localAddr.str().c_str());
    request->setSourcePort(localPort);
    request->setByteLength(1024);

    sendPacket(request);

}

void VehicleProxyLayer::connect(const char *addr, int port)
{
    socket.renewSocket();
    const char *connectAddress = addr;
    int connectPort = port;


    L3Address destination;
    L3AddressResolver().tryResolve(connectAddress, destination);
    if (destination.isUnspecified()) {
        EV_ERROR << "Connecting to " << connectAddress << " port=" << connectPort << ": cannot resolve destination address\n";
    }

    EV_INFO << "Connecting to " << connectAddress << "(" << destination << ") port=" << connectPort << endl;

    socket.connect(destination, connectPort);

}


void VehicleProxyLayer::stopListening()
{
    socket.close();
}

void VehicleProxyLayer::sendDown(cMessage *msg)
{
    if (msg->isPacket()) {
        bytesSent += ((cPacket *)msg)->getByteLength();
        emit(sentPkSignal, (cPacket *)msg);
    }
    send(msg, "tcpOut");
}

void VehicleProxyLayer::handleMessage(cMessage *msg)
{
    std::string verify (msg->getClassName());

    if(verify.compare("OpenFlowMsg") == 0){
        processOfData(msg);
    }
    else if(verify.compare("StatusMsg") == 0){
        processStatusData(msg);
    }

    if (!isNodeUp())
        throw cRuntimeError("Application is not running");
    if (msg->isSelfMessage()) {
        sendDown(msg);
    }
    else if (msg->getKind() == TCP_I_PEER_CLOSED) {
        // we'll close too
        msg->setName("close");
        msg->setKind(TCP_C_CLOSE);

        if (delay == 0)
            sendDown(msg);
        else
            scheduleAt(simTime() + delay, msg); // send after a delay
    }
    else if (msg->getKind() == TCP_I_URGENT_DATA) {

        cPacket *pkt = check_and_cast<cPacket *>(msg);
        emit(rcvdPkSignal, pkt);
        bytesRcvd += pkt->getByteLength();
        delete msg;

    }else if(msg->getKind() == TCP_I_DATA){
        //send(msg, "upperAppOut");
    }
    else {
        delete msg;
    }
}

void VehicleProxyLayer::refreshDisplay() const
{
    char buf[80];
    sprintf(buf, "rcvd: %ld bytes\nsent: %ld bytes", bytesRcvd, bytesSent);
    getDisplayString().setTagArg("t", 0, buf);
}

bool VehicleProxyLayer::handleOperationStage(LifecycleOperation *operation, int stage, IDoneCallback *doneCallback)
{
    Enter_Method_Silent();
    if (dynamic_cast<NodeStartOperation *>(operation)) {
        if ((NodeStartOperation::Stage)stage == NodeStartOperation::STAGE_APPLICATION_LAYER)
            startListening();
    }
    else if (dynamic_cast<NodeShutdownOperation *>(operation)) {
        if ((NodeShutdownOperation::Stage)stage == NodeShutdownOperation::STAGE_APPLICATION_LAYER)
            // TODO: wait until socket is closed
            stopListening();
    }
    else if (dynamic_cast<NodeCrashOperation *>(operation))
        ;
    else
        throw cRuntimeError("Unsupported lifecycle operation '%s'", operation->getClassName());
    return true;
}


void VehicleProxyLayer::finish()
{
    //recordScalar("bytesRcvd", bytesRcvd);
    //recordScalar("bytesSent", bytesSent);
}

}

