#ifndef __VEHICLEPROXYLAYER_H
#define __VEHICLEPROXYLAYER_H

#include "inet/common/INETDefs.h"
#include "inet/common/INETMath.h"
#include "inet/common/lifecycle/ILifecycle.h"
#include "inet/common/lifecycle/NodeStatus.h"
#include "inet/transportlayer/contract/tcp/TCPSocket.h"
#include "messages/StatusMsg_m.h"
#include "messages/OpenFlowMsg_m.h"
#include "messages/MessageStructures.h"

namespace inet {

class VehicleProxyLayer : public cSimpleModule, public ILifecycle
{
  protected:
    std::string lCAddrConn;
    int lCPortConn;
    double lessDistance = 0.0;
    int verify = 1;
    bool usingQoS;
    int appTYPE;

  protected:
    simtime_t delay;
    double echoFactor = NaN;

    TCPSocket socket;
    NodeStatus *nodeStatus = nullptr;

    long bytesRcvd = 0;
    long bytesSent = 0;

    double distance;

    static simsignal_t rcvdPkSignal;
    static simsignal_t sentPkSignal;

  protected:
    virtual bool isNodeUp();
    virtual void sendDown(cMessage *msg);
    virtual void startListening();
    virtual void stopListening();
    virtual void connect(const char *addr, int port);

    virtual void initialize(int stage) override;
    virtual int numInitStages() const override { return NUM_INIT_STAGES; }
    virtual void handleMessage(cMessage *msg) override;
    virtual void finish() override;
    virtual void refreshDisplay() const override;
    virtual bool handleOperationStage(LifecycleOperation *operation, int stage, IDoneCallback *doneCallback) override;

  protected:
    virtual void processOfData(cMessage *msg);
    virtual void processStatusData(cMessage *msg);
    virtual void verifyPositionLC();
    virtual void sendPacket(cPacket *msg);
    virtual void calculateDistance(string lCaddress, int lCPort, double lcPosX, double lcPosY);
    virtual void processCCRVI(OpenFlowMsg *status);
    virtual void processFlowTableRequest(StatusMsg *status);
  public:
    VehicleProxyLayer() {}
};

}

#endif
