#ifndef __CONTRTCPAPP_H
#define __CONTRTCPAPP_H

#include "inet/common/INETDefs.h"
#include "inet/common/INETMath.h"
#include "inet/common/lifecycle/ILifecycle.h"
#include "inet/common/lifecycle/NodeStatus.h"
#include "inet/transportlayer/contract/tcp/TCPSocket.h"
#include "messages/OpenFlowMsg_m.h"

namespace inet {

class LocalControllerProxyLayer : public cSimpleModule, public ILifecycle
{
  protected:
    simtime_t delay;
    double echoFactor = NaN;

    int wwwPortServer;
    const char *addrServer;
    int ftpPortServer;
    const char *rtpServer;
    int rtpPort;

    TCPSocket socket;
    NodeStatus *nodeStatus = nullptr;

    // statistics
    long bytesRcvd = 0;
    long bytesSent = 0;
    int numSessions;
    int numBroken;
    int packetsSent;
    int packetsRcvd;


    //statistics:
    static simsignal_t connectSignal;
    static simsignal_t rcvdPkSignal;
    static simsignal_t sentPkSignal;

  protected:
    OpenFlowMsg *receive = nullptr;
    OpenFlowMsg *response = nullptr;

  protected:
      virtual void sendBack(cMessage *msg);
      virtual void sendLocalControllerInformations();
      virtual void connect();
      virtual void sendPacket(cPacket *msg);

  protected:
    virtual bool isNodeUp();
    virtual void sendDown(cMessage *msg);
    virtual void startListening();
    virtual void stopListening();

    virtual void initialize(int stage) override;
    virtual int numInitStages() const override { return NUM_INIT_STAGES; }
    virtual void handleMessage(cMessage *msg) override;
    virtual void finish() override;
    virtual void refreshDisplay() const override;
    virtual bool handleOperationStage(LifecycleOperation *operation, int stage, IDoneCallback *doneCallback) override;

  public:
    LocalControllerProxyLayer() {}
};

} // namespace inet

#endif
