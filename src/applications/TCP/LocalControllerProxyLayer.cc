#include "LocalControllerProxyLayer.h"
#include "inet/common/RawPacket.h"
#include "inet/transportlayer/contract/tcp/TCPCommand_m.h"
#include "inet/common/ModuleAccess.h"
#include "inet/common/lifecycle/NodeOperations.h"
#include "LocalControllerProxyLayer.h"
#include "messages/OpenFlowMsg_m.h"
#include "inet/mobility/contract/IMobility.h"
#include "inet/networklayer/common/L3AddressResolver.h"


namespace inet {

Define_Module(LocalControllerProxyLayer);

simsignal_t LocalControllerProxyLayer::rcvdPkSignal = registerSignal("rcvdPk");
simsignal_t LocalControllerProxyLayer::sentPkSignal = registerSignal("sentPk");

void LocalControllerProxyLayer::initialize(int stage)
{
    cSimpleModule::initialize(stage);

    if (stage == INITSTAGE_LOCAL) {
        numSessions = numBroken = packetsSent = packetsRcvd = bytesSent = bytesRcvd = 0;

        wwwPortServer = par("wwwPortServer");
        addrServer = par("addrServer");
        ftpPortServer = par("ftpPortServer");
        rtpServer = par("rtpServer");
        rtpPort = par("rtpPort");

        delay = par("echoDelay");
        echoFactor = par("echoFactor");

        bytesRcvd = bytesSent = 0;
        WATCH(bytesRcvd);
        WATCH(bytesSent);

        WATCH(numSessions);
        WATCH(numBroken);
        WATCH(packetsSent);
        WATCH(packetsRcvd);

        socket.setOutputGate(gate("tcpOut"));
        socket.readDataTransferModePar(*this);

        nodeStatus = dynamic_cast<NodeStatus *>(findContainingNode(this)->getSubmodule("status"));
    }
    else if (stage == INITSTAGE_APPLICATION_LAYER) {
        if (isNodeUp())
            connect();
            sendLocalControllerInformations();
            startListening();
    }

}

void LocalControllerProxyLayer::handleMessage(cMessage *msg)
{
        std::string verify (msg->getClassName());
        std::string gate = msg->getArrivalGate()->getBaseName();

        if (!isNodeUp())
            throw cRuntimeError("Application is not running");
        if (msg->isSelfMessage()) {
            sendDown(msg);
        }

        if(verify.compare("OpenFlowMsg") == 0){
                if(gate.compare("tcpIn") == 0){

                    receive = check_and_cast<OpenFlowMsg *>(msg);
                    emit(rcvdPkSignal, receive);
                    bytesRcvd += receive->getByteLength();

                    send(msg, "upperTCPAppOut");
                }else if(gate.compare("upperTCPAppIn") == 0){
                    sendBack(msg);
                }

            }
        else if (msg->getKind() == TCP_I_PEER_CLOSED) {
            // we'll close too
            msg->setName("close");
            msg->setKind(TCP_C_CLOSE);

            if (delay == 0)
                sendDown(msg);
            else
                scheduleAt(simTime() + delay, msg); // send after a delay
        }

        else {
            // some indication -- ignore
            delete msg;
        }
}

void LocalControllerProxyLayer::sendLocalControllerInformations(){

    cModule *host = getContainingNode(this);
    IMobility *mod = check_and_cast<IMobility *>(host->getSubmodule("mobility"));
    Coord pos = mod->getCurrentPosition();

    int localPort = par("localPort");
    double coord_X = pos.x;
    double coord_Y = pos.y;
    double coord_Z = pos.z;

    L3Address localAddr = L3AddressResolver().resolve(host->getFullName());

    OpenFlowMsg *ofm = new OpenFlowMsg("LocalControllerInfos");
    ofm->setOFtype(OFPT_CONTROLLER_STATUS);
    ofm->setNodeName(host->getFullName());
    ofm->setSourceAddress(localAddr.str().c_str());
    ofm->setSourcePort(localPort);
    ofm->setPosX(coord_X); //Geographic informations
    ofm->setPosY(coord_Y);
    ofm->setPosZ(coord_Z);
    ofm->setByteLength(100);

    sendPacket(ofm);
}

void LocalControllerProxyLayer::sendBack(cMessage *msg)
{
    response = check_and_cast<OpenFlowMsg *>(msg);

    int appType = response->getAppType();

    response->setDestAddress(addrServer);

    if(appType == 1){
        response->setDestAddress(rtpServer);
        response->setDestPort(rtpPort);
    }
    if(appType == 2){ //WWW
        response->setDestPort(wwwPortServer);
    }
    else if(appType == 3){ //FTP
        response->setDestPort(ftpPortServer);
    }

    emit(rcvdPkSignal, response);
    bytesRcvd += response->getByteLength();
    int connId = check_and_cast<TCPCommand *>(receive->getControlInfo())->getConnId();
    delete response->removeControlInfo();

    TCPSendCommand *cmd = new TCPSendCommand();
    cmd->setConnId(connId);
    response->setControlInfo(cmd);
    response->setKind(TCP_C_SEND);
    response->setByteLength(1024);
    sendDown(response);

}


bool LocalControllerProxyLayer::isNodeUp()
{
    return !nodeStatus || nodeStatus->getState() == NodeStatus::UP;
}

void LocalControllerProxyLayer::startListening()
{
    const char *localAddress = par("localAddress");
    int localPort = par("localPort");
    socket.renewSocket();
    socket.bind(localAddress[0] ? L3Address(localAddress) : L3Address(), localPort);
    socket.listen();
}

void LocalControllerProxyLayer::connect()
{
    socket.renewSocket();
    const char *connectAddress =  par("centralControllerAddress");
    int connectPort = par("centralControllerPort");

    L3Address destination;
    L3AddressResolver().tryResolve(connectAddress, destination);
    if (destination.isUnspecified()) {
        EV_ERROR << "Connecting to " << connectAddress << " port=" << connectPort << ": cannot resolve destination address\n";
    }

    EV_INFO << "Connecting to " << connectAddress << "(" << destination << ") port=" << connectPort << endl;

    socket.connect(destination, connectPort);

}

void LocalControllerProxyLayer::sendPacket(cPacket *msg)
{
    int numBytes = msg->getByteLength();
    emit(sentPkSignal, msg);
    socket.send(msg);

    packetsSent++;
    bytesSent += numBytes;
}


void LocalControllerProxyLayer::stopListening()
{
    socket.close();
}

void LocalControllerProxyLayer::sendDown(cMessage *msg)
{
    cPacket *packet = dynamic_cast<cPacket *>(msg);

    if (packet) {
        bytesSent += packet->getByteLength();
        emit(sentPkSignal, packet);

        EV_INFO << "sending \"" << packet->getName() << "\" to TCP, " << packet->getByteLength() << " bytes\n";
    }
    else {
        EV_INFO << "sending \"" << msg->getName() << "\" to TCP\n";
    }

    send(msg, "tcpOut");
}

void LocalControllerProxyLayer::refreshDisplay() const
{
    char buf[80];
    sprintf(buf, "rcvd: %ld bytes\nsent: %ld bytes", bytesRcvd, bytesSent);
    getDisplayString().setTagArg("t", 0, buf);
}

bool LocalControllerProxyLayer::handleOperationStage(LifecycleOperation *operation, int stage, IDoneCallback *doneCallback)
{
    Enter_Method_Silent();
    if (dynamic_cast<NodeStartOperation *>(operation)) {
        if ((NodeStartOperation::Stage)stage == NodeStartOperation::STAGE_APPLICATION_LAYER)
            startListening();
    }
    else if (dynamic_cast<NodeShutdownOperation *>(operation)) {
        if ((NodeShutdownOperation::Stage)stage == NodeShutdownOperation::STAGE_APPLICATION_LAYER)
            // TODO: wait until socket is closed
            stopListening();
    }
    else if (dynamic_cast<NodeCrashOperation *>(operation))
        ;
    else
        throw cRuntimeError("Unsupported lifecycle operation '%s'", operation->getClassName());
    return true;

}

void LocalControllerProxyLayer::finish()
{
    //recordScalar("bytesRcvd", bytesRcvd);
    //recordScalar("bytesSent", bytesSent);
}

} // namespace inet

