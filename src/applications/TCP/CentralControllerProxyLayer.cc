
#include "CentralControllerProxyLayer.h"

#include "inet/common/RawPacket.h"
#include "inet/transportlayer/contract/tcp/TCPCommand_m.h"
#include "inet/common/ModuleAccess.h"
#include "inet/common/lifecycle/NodeOperations.h"
#include "messages/OpenFlowMsg_m.h"

namespace inet {

Define_Module(CentralControllerProxyLayer);

simsignal_t CentralControllerProxyLayer::rcvdPkSignal = registerSignal("rcvdPk");
simsignal_t CentralControllerProxyLayer::sentPkSignal = registerSignal("sentPk");

void CentralControllerProxyLayer::initialize(int stage)
{
    cSimpleModule::initialize(stage);

    if (stage == INITSTAGE_LOCAL) {
        numSessions = numBroken = packetsSent = packetsRcvd = bytesSent = bytesRcvd = 0;

        delay = par("echoDelay");
        echoFactor = par("echoFactor");

        bytesRcvd = bytesSent = 0;
        WATCH(bytesRcvd);
        WATCH(bytesSent);

        WATCH(numSessions);
        WATCH(numBroken);
        WATCH(packetsSent);
        WATCH(packetsRcvd);

        socket.setOutputGate(gate("tcpOut"));
        socket.readDataTransferModePar(*this);

        nodeStatus = dynamic_cast<NodeStatus *>(findContainingNode(this)->getSubmodule("status"));
    }
    else if (stage == INITSTAGE_APPLICATION_LAYER) {
        if (isNodeUp())
            startListening();

    }
}

bool CentralControllerProxyLayer::isNodeUp()
{
    return !nodeStatus || nodeStatus->getState() == NodeStatus::UP;
}

void CentralControllerProxyLayer::startListening()
{
    const char *localAddress = par("localAddress");
    int localPort = par("localPort");
    socket.renewSocket();
    socket.bind(localAddress[0] ? L3Address(localAddress) : L3Address(), localPort);
    socket.listen();
}

void CentralControllerProxyLayer::stopListening()
{
    socket.close();
}

void CentralControllerProxyLayer::sendDown(cMessage *msg)
{
    cPacket *packet = dynamic_cast<cPacket *>(msg);

    if (packet) {
            //msgsSent++;
            bytesSent += packet->getByteLength();
            emit(sentPkSignal, packet);

            EV_INFO << "sending \"" << packet->getName() << "\" to TCP, " << packet->getByteLength() << " bytes\n";
         }
    else {
            EV_INFO << "sending \"" << msg->getName() << "\" to TCP\n";
    }

    send(msg, "tcpOut");
}

void CentralControllerProxyLayer::handleMessage(cMessage *msg)
{
    std::string verify (msg->getClassName());
    //cModule *host = nullptr;
    //host = getContainingNode(this);
    std::string gate = msg->getArrivalGate()->getBaseName();

    if (!isNodeUp())
        throw cRuntimeError("Application is not running");
    if (msg->isSelfMessage()) {
        sendDown(msg);
    }

    if(verify.compare("OpenFlowMsg") == 0){
        if(gate.compare("tcpIn") == 0){

            receive = check_and_cast<OpenFlowMsg *>(msg);
            emit(rcvdPkSignal, receive);
            bytesRcvd += receive->getByteLength();

            send(msg, "upperTCPAppOut");
        }else if(gate.compare("upperTCPAppIn") == 0){
            sendBack(msg);
        }

    }
    else if (msg->getKind() == TCP_I_PEER_CLOSED) {
        // we'll close too
        msg->setName("close");
        msg->setKind(TCP_C_CLOSE);

        if (delay == 0)
            sendDown(msg);
        else
            scheduleAt(simTime() + delay, msg); // send after a delay
    }

    else {
        // some indication -- ignore
        delete msg;
    }
}

void CentralControllerProxyLayer::sendBack(cMessage *msg)
{
    response = check_and_cast<OpenFlowMsg *>(msg);
    emit(rcvdPkSignal, response);
    bytesRcvd += response->getByteLength();


    int connId = check_and_cast<TCPCommand *>(receive->getControlInfo())->getConnId();
    delete response->removeControlInfo();

    TCPSendCommand *cmd = new TCPSendCommand();
    cmd->setConnId(connId);
    response->setControlInfo(cmd);
    response->setKind(TCP_C_SEND);
    response->setByteLength(1024);

    sendDown(response);


}

void CentralControllerProxyLayer::refreshDisplay() const
{
    char buf[80];
    sprintf(buf, "rcvd: %ld bytes\nsent: %ld bytes", bytesRcvd, bytesSent);
    getDisplayString().setTagArg("t", 0, buf);
}

bool CentralControllerProxyLayer::handleOperationStage(LifecycleOperation *operation, int stage, IDoneCallback *doneCallback)
{
    Enter_Method_Silent();
    if (dynamic_cast<NodeStartOperation *>(operation)) {
        if ((NodeStartOperation::Stage)stage == NodeStartOperation::STAGE_APPLICATION_LAYER)
            startListening();
    }
    else if (dynamic_cast<NodeShutdownOperation *>(operation)) {
        if ((NodeShutdownOperation::Stage)stage == NodeShutdownOperation::STAGE_APPLICATION_LAYER)
            // TODO: wait until socket is closed
            stopListening();
    }
    else if (dynamic_cast<NodeCrashOperation *>(operation))
        ;
    else
        throw cRuntimeError("Unsupported lifecycle operation '%s'", operation->getClassName());
    return true;
}

void CentralControllerProxyLayer::finish()
{
    //recordScalar("bytesRcvd", bytesRcvd);
    //recordScalar("bytesSent", bytesSent);
}

void CentralControllerProxyLayer::testaCodigo(std::string msg)
{
    std::cout << "INFO: " << msg << std::endl;
}

} // namespace inet

