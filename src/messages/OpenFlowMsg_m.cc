//
// Generated file, do not edit! Created by nedtool 5.1 from messages/OpenFlowMsg.msg.
//

// Disable warnings about unused variables, empty switch stmts, etc:
#ifdef _MSC_VER
#  pragma warning(disable:4101)
#  pragma warning(disable:4065)
#endif

#if defined(__clang__)
#  pragma clang diagnostic ignored "-Wshadow"
#  pragma clang diagnostic ignored "-Wconversion"
#  pragma clang diagnostic ignored "-Wunused-parameter"
#  pragma clang diagnostic ignored "-Wc++98-compat"
#  pragma clang diagnostic ignored "-Wunreachable-code-break"
#  pragma clang diagnostic ignored "-Wold-style-cast"
#elif defined(__GNUC__)
#  pragma GCC diagnostic ignored "-Wshadow"
#  pragma GCC diagnostic ignored "-Wconversion"
#  pragma GCC diagnostic ignored "-Wunused-parameter"
#  pragma GCC diagnostic ignored "-Wold-style-cast"
#  pragma GCC diagnostic ignored "-Wsuggest-attribute=noreturn"
#  pragma GCC diagnostic ignored "-Wfloat-conversion"
#endif

#include <iostream>
#include <sstream>
#include "OpenFlowMsg_m.h"

namespace omnetpp {

// Template pack/unpack rules. They are declared *after* a1l type-specific pack functions for multiple reasons.
// They are in the omnetpp namespace, to allow them to be found by argument-dependent lookup via the cCommBuffer argument

// Packing/unpacking an std::vector
template<typename T, typename A>
void doParsimPacking(omnetpp::cCommBuffer *buffer, const std::vector<T,A>& v)
{
    int n = v.size();
    doParsimPacking(buffer, n);
    for (int i = 0; i < n; i++)
        doParsimPacking(buffer, v[i]);
}

template<typename T, typename A>
void doParsimUnpacking(omnetpp::cCommBuffer *buffer, std::vector<T,A>& v)
{
    int n;
    doParsimUnpacking(buffer, n);
    v.resize(n);
    for (int i = 0; i < n; i++)
        doParsimUnpacking(buffer, v[i]);
}

// Packing/unpacking an std::list
template<typename T, typename A>
void doParsimPacking(omnetpp::cCommBuffer *buffer, const std::list<T,A>& l)
{
    doParsimPacking(buffer, (int)l.size());
    for (typename std::list<T,A>::const_iterator it = l.begin(); it != l.end(); ++it)
        doParsimPacking(buffer, (T&)*it);
}

template<typename T, typename A>
void doParsimUnpacking(omnetpp::cCommBuffer *buffer, std::list<T,A>& l)
{
    int n;
    doParsimUnpacking(buffer, n);
    for (int i=0; i<n; i++) {
        l.push_back(T());
        doParsimUnpacking(buffer, l.back());
    }
}

// Packing/unpacking an std::set
template<typename T, typename Tr, typename A>
void doParsimPacking(omnetpp::cCommBuffer *buffer, const std::set<T,Tr,A>& s)
{
    doParsimPacking(buffer, (int)s.size());
    for (typename std::set<T,Tr,A>::const_iterator it = s.begin(); it != s.end(); ++it)
        doParsimPacking(buffer, *it);
}

template<typename T, typename Tr, typename A>
void doParsimUnpacking(omnetpp::cCommBuffer *buffer, std::set<T,Tr,A>& s)
{
    int n;
    doParsimUnpacking(buffer, n);
    for (int i=0; i<n; i++) {
        T x;
        doParsimUnpacking(buffer, x);
        s.insert(x);
    }
}

// Packing/unpacking an std::map
template<typename K, typename V, typename Tr, typename A>
void doParsimPacking(omnetpp::cCommBuffer *buffer, const std::map<K,V,Tr,A>& m)
{
    doParsimPacking(buffer, (int)m.size());
    for (typename std::map<K,V,Tr,A>::const_iterator it = m.begin(); it != m.end(); ++it) {
        doParsimPacking(buffer, it->first);
        doParsimPacking(buffer, it->second);
    }
}

template<typename K, typename V, typename Tr, typename A>
void doParsimUnpacking(omnetpp::cCommBuffer *buffer, std::map<K,V,Tr,A>& m)
{
    int n;
    doParsimUnpacking(buffer, n);
    for (int i=0; i<n; i++) {
        K k; V v;
        doParsimUnpacking(buffer, k);
        doParsimUnpacking(buffer, v);
        m[k] = v;
    }
}

// Default pack/unpack function for arrays
template<typename T>
void doParsimArrayPacking(omnetpp::cCommBuffer *b, const T *t, int n)
{
    for (int i = 0; i < n; i++)
        doParsimPacking(b, t[i]);
}

template<typename T>
void doParsimArrayUnpacking(omnetpp::cCommBuffer *b, T *t, int n)
{
    for (int i = 0; i < n; i++)
        doParsimUnpacking(b, t[i]);
}

// Default rule to prevent compiler from choosing base class' doParsimPacking() function
template<typename T>
void doParsimPacking(omnetpp::cCommBuffer *, const T& t)
{
    throw omnetpp::cRuntimeError("Parsim error: No doParsimPacking() function for type %s", omnetpp::opp_typename(typeid(t)));
}

template<typename T>
void doParsimUnpacking(omnetpp::cCommBuffer *, T& t)
{
    throw omnetpp::cRuntimeError("Parsim error: No doParsimUnpacking() function for type %s", omnetpp::opp_typename(typeid(t)));
}

}  // namespace omnetpp


// forward
template<typename T, typename A>
std::ostream& operator<<(std::ostream& out, const std::vector<T,A>& vec);

// Template rule which fires if a struct or class doesn't have operator<<
template<typename T>
inline std::ostream& operator<<(std::ostream& out,const T&) {return out;}

// operator<< for std::vector<T>
template<typename T, typename A>
inline std::ostream& operator<<(std::ostream& out, const std::vector<T,A>& vec)
{
    out.put('{');
    for(typename std::vector<T,A>::const_iterator it = vec.begin(); it != vec.end(); ++it)
    {
        if (it != vec.begin()) {
            out.put(','); out.put(' ');
        }
        out << *it;
    }
    out.put('}');
    
    char buf[32];
    sprintf(buf, " (size=%u)", (unsigned int)vec.size());
    out.write(buf, strlen(buf));
    return out;
}

Register_Class(OpenFlowMsg)

OpenFlowMsg::OpenFlowMsg(const char *name, short kind) : ::omnetpp::cPacket(name,kind)
{
    this->OFtype = 0;
    this->appType = 0;
    this->sourcePort = 0;
    this->destPort = 0;
    this->posX = 0;
    this->posY = 0;
    this->posZ = 0;
}

OpenFlowMsg::OpenFlowMsg(const OpenFlowMsg& other) : ::omnetpp::cPacket(other)
{
    copy(other);
}

OpenFlowMsg::~OpenFlowMsg()
{
}

OpenFlowMsg& OpenFlowMsg::operator=(const OpenFlowMsg& other)
{
    if (this==&other) return *this;
    ::omnetpp::cPacket::operator=(other);
    copy(other);
    return *this;
}

void OpenFlowMsg::copy(const OpenFlowMsg& other)
{
    this->OFtype = other.OFtype;
    this->appType = other.appType;
    this->nodeName = other.nodeName;
    this->sourceAddress = other.sourceAddress;
    this->sourcePort = other.sourcePort;
    this->destAddress = other.destAddress;
    this->destPort = other.destPort;
    this->posX = other.posX;
    this->posY = other.posY;
    this->posZ = other.posZ;
    this->info = other.info;
    this->rule = other.rule;
}

void OpenFlowMsg::parsimPack(omnetpp::cCommBuffer *b) const
{
    ::omnetpp::cPacket::parsimPack(b);
    doParsimPacking(b,this->OFtype);
    doParsimPacking(b,this->appType);
    doParsimPacking(b,this->nodeName);
    doParsimPacking(b,this->sourceAddress);
    doParsimPacking(b,this->sourcePort);
    doParsimPacking(b,this->destAddress);
    doParsimPacking(b,this->destPort);
    doParsimPacking(b,this->posX);
    doParsimPacking(b,this->posY);
    doParsimPacking(b,this->posZ);
    doParsimPacking(b,this->info);
    doParsimPacking(b,this->rule);
}

void OpenFlowMsg::parsimUnpack(omnetpp::cCommBuffer *b)
{
    ::omnetpp::cPacket::parsimUnpack(b);
    doParsimUnpacking(b,this->OFtype);
    doParsimUnpacking(b,this->appType);
    doParsimUnpacking(b,this->nodeName);
    doParsimUnpacking(b,this->sourceAddress);
    doParsimUnpacking(b,this->sourcePort);
    doParsimUnpacking(b,this->destAddress);
    doParsimUnpacking(b,this->destPort);
    doParsimUnpacking(b,this->posX);
    doParsimUnpacking(b,this->posY);
    doParsimUnpacking(b,this->posZ);
    doParsimUnpacking(b,this->info);
    doParsimUnpacking(b,this->rule);
}

int OpenFlowMsg::getOFtype() const
{
    return this->OFtype;
}

void OpenFlowMsg::setOFtype(int OFtype)
{
    this->OFtype = OFtype;
}

int OpenFlowMsg::getAppType() const
{
    return this->appType;
}

void OpenFlowMsg::setAppType(int appType)
{
    this->appType = appType;
}

const char * OpenFlowMsg::getNodeName() const
{
    return this->nodeName.c_str();
}

void OpenFlowMsg::setNodeName(const char * nodeName)
{
    this->nodeName = nodeName;
}

const char * OpenFlowMsg::getSourceAddress() const
{
    return this->sourceAddress.c_str();
}

void OpenFlowMsg::setSourceAddress(const char * sourceAddress)
{
    this->sourceAddress = sourceAddress;
}

int OpenFlowMsg::getSourcePort() const
{
    return this->sourcePort;
}

void OpenFlowMsg::setSourcePort(int sourcePort)
{
    this->sourcePort = sourcePort;
}

const char * OpenFlowMsg::getDestAddress() const
{
    return this->destAddress.c_str();
}

void OpenFlowMsg::setDestAddress(const char * destAddress)
{
    this->destAddress = destAddress;
}

int OpenFlowMsg::getDestPort() const
{
    return this->destPort;
}

void OpenFlowMsg::setDestPort(int destPort)
{
    this->destPort = destPort;
}

double OpenFlowMsg::getPosX() const
{
    return this->posX;
}

void OpenFlowMsg::setPosX(double posX)
{
    this->posX = posX;
}

double OpenFlowMsg::getPosY() const
{
    return this->posY;
}

void OpenFlowMsg::setPosY(double posY)
{
    this->posY = posY;
}

double OpenFlowMsg::getPosZ() const
{
    return this->posZ;
}

void OpenFlowMsg::setPosZ(double posZ)
{
    this->posZ = posZ;
}

localControllerInformations& OpenFlowMsg::getInfo()
{
    return this->info;
}

void OpenFlowMsg::setInfo(const localControllerInformations& info)
{
    this->info = info;
}

ofp_flow_table_header_fields& OpenFlowMsg::getRule()
{
    return this->rule;
}

void OpenFlowMsg::setRule(const ofp_flow_table_header_fields& rule)
{
    this->rule = rule;
}

class OpenFlowMsgDescriptor : public omnetpp::cClassDescriptor
{
  private:
    mutable const char **propertynames;
  public:
    OpenFlowMsgDescriptor();
    virtual ~OpenFlowMsgDescriptor();

    virtual bool doesSupport(omnetpp::cObject *obj) const override;
    virtual const char **getPropertyNames() const override;
    virtual const char *getProperty(const char *propertyname) const override;
    virtual int getFieldCount() const override;
    virtual const char *getFieldName(int field) const override;
    virtual int findField(const char *fieldName) const override;
    virtual unsigned int getFieldTypeFlags(int field) const override;
    virtual const char *getFieldTypeString(int field) const override;
    virtual const char **getFieldPropertyNames(int field) const override;
    virtual const char *getFieldProperty(int field, const char *propertyname) const override;
    virtual int getFieldArraySize(void *object, int field) const override;

    virtual const char *getFieldDynamicTypeString(void *object, int field, int i) const override;
    virtual std::string getFieldValueAsString(void *object, int field, int i) const override;
    virtual bool setFieldValueAsString(void *object, int field, int i, const char *value) const override;

    virtual const char *getFieldStructName(int field) const override;
    virtual void *getFieldStructValuePointer(void *object, int field, int i) const override;
};

Register_ClassDescriptor(OpenFlowMsgDescriptor)

OpenFlowMsgDescriptor::OpenFlowMsgDescriptor() : omnetpp::cClassDescriptor("OpenFlowMsg", "omnetpp::cPacket")
{
    propertynames = nullptr;
}

OpenFlowMsgDescriptor::~OpenFlowMsgDescriptor()
{
    delete[] propertynames;
}

bool OpenFlowMsgDescriptor::doesSupport(omnetpp::cObject *obj) const
{
    return dynamic_cast<OpenFlowMsg *>(obj)!=nullptr;
}

const char **OpenFlowMsgDescriptor::getPropertyNames() const
{
    if (!propertynames) {
        static const char *names[] = {  nullptr };
        omnetpp::cClassDescriptor *basedesc = getBaseClassDescriptor();
        const char **basenames = basedesc ? basedesc->getPropertyNames() : nullptr;
        propertynames = mergeLists(basenames, names);
    }
    return propertynames;
}

const char *OpenFlowMsgDescriptor::getProperty(const char *propertyname) const
{
    omnetpp::cClassDescriptor *basedesc = getBaseClassDescriptor();
    return basedesc ? basedesc->getProperty(propertyname) : nullptr;
}

int OpenFlowMsgDescriptor::getFieldCount() const
{
    omnetpp::cClassDescriptor *basedesc = getBaseClassDescriptor();
    return basedesc ? 12+basedesc->getFieldCount() : 12;
}

unsigned int OpenFlowMsgDescriptor::getFieldTypeFlags(int field) const
{
    omnetpp::cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount())
            return basedesc->getFieldTypeFlags(field);
        field -= basedesc->getFieldCount();
    }
    static unsigned int fieldTypeFlags[] = {
        FD_ISEDITABLE,
        FD_ISEDITABLE,
        FD_ISEDITABLE,
        FD_ISEDITABLE,
        FD_ISEDITABLE,
        FD_ISEDITABLE,
        FD_ISEDITABLE,
        FD_ISEDITABLE,
        FD_ISEDITABLE,
        FD_ISEDITABLE,
        FD_ISCOMPOUND,
        FD_ISCOMPOUND,
    };
    return (field>=0 && field<12) ? fieldTypeFlags[field] : 0;
}

const char *OpenFlowMsgDescriptor::getFieldName(int field) const
{
    omnetpp::cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount())
            return basedesc->getFieldName(field);
        field -= basedesc->getFieldCount();
    }
    static const char *fieldNames[] = {
        "OFtype",
        "appType",
        "nodeName",
        "sourceAddress",
        "sourcePort",
        "destAddress",
        "destPort",
        "posX",
        "posY",
        "posZ",
        "info",
        "rule",
    };
    return (field>=0 && field<12) ? fieldNames[field] : nullptr;
}

int OpenFlowMsgDescriptor::findField(const char *fieldName) const
{
    omnetpp::cClassDescriptor *basedesc = getBaseClassDescriptor();
    int base = basedesc ? basedesc->getFieldCount() : 0;
    if (fieldName[0]=='O' && strcmp(fieldName, "OFtype")==0) return base+0;
    if (fieldName[0]=='a' && strcmp(fieldName, "appType")==0) return base+1;
    if (fieldName[0]=='n' && strcmp(fieldName, "nodeName")==0) return base+2;
    if (fieldName[0]=='s' && strcmp(fieldName, "sourceAddress")==0) return base+3;
    if (fieldName[0]=='s' && strcmp(fieldName, "sourcePort")==0) return base+4;
    if (fieldName[0]=='d' && strcmp(fieldName, "destAddress")==0) return base+5;
    if (fieldName[0]=='d' && strcmp(fieldName, "destPort")==0) return base+6;
    if (fieldName[0]=='p' && strcmp(fieldName, "posX")==0) return base+7;
    if (fieldName[0]=='p' && strcmp(fieldName, "posY")==0) return base+8;
    if (fieldName[0]=='p' && strcmp(fieldName, "posZ")==0) return base+9;
    if (fieldName[0]=='i' && strcmp(fieldName, "info")==0) return base+10;
    if (fieldName[0]=='r' && strcmp(fieldName, "rule")==0) return base+11;
    return basedesc ? basedesc->findField(fieldName) : -1;
}

const char *OpenFlowMsgDescriptor::getFieldTypeString(int field) const
{
    omnetpp::cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount())
            return basedesc->getFieldTypeString(field);
        field -= basedesc->getFieldCount();
    }
    static const char *fieldTypeStrings[] = {
        "int",
        "int",
        "string",
        "string",
        "int",
        "string",
        "int",
        "double",
        "double",
        "double",
        "localControllerInformations",
        "ofp_flow_table_header_fields",
    };
    return (field>=0 && field<12) ? fieldTypeStrings[field] : nullptr;
}

const char **OpenFlowMsgDescriptor::getFieldPropertyNames(int field) const
{
    omnetpp::cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount())
            return basedesc->getFieldPropertyNames(field);
        field -= basedesc->getFieldCount();
    }
    switch (field) {
        default: return nullptr;
    }
}

const char *OpenFlowMsgDescriptor::getFieldProperty(int field, const char *propertyname) const
{
    omnetpp::cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount())
            return basedesc->getFieldProperty(field, propertyname);
        field -= basedesc->getFieldCount();
    }
    switch (field) {
        default: return nullptr;
    }
}

int OpenFlowMsgDescriptor::getFieldArraySize(void *object, int field) const
{
    omnetpp::cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount())
            return basedesc->getFieldArraySize(object, field);
        field -= basedesc->getFieldCount();
    }
    OpenFlowMsg *pp = (OpenFlowMsg *)object; (void)pp;
    switch (field) {
        default: return 0;
    }
}

const char *OpenFlowMsgDescriptor::getFieldDynamicTypeString(void *object, int field, int i) const
{
    omnetpp::cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount())
            return basedesc->getFieldDynamicTypeString(object,field,i);
        field -= basedesc->getFieldCount();
    }
    OpenFlowMsg *pp = (OpenFlowMsg *)object; (void)pp;
    switch (field) {
        default: return nullptr;
    }
}

std::string OpenFlowMsgDescriptor::getFieldValueAsString(void *object, int field, int i) const
{
    omnetpp::cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount())
            return basedesc->getFieldValueAsString(object,field,i);
        field -= basedesc->getFieldCount();
    }
    OpenFlowMsg *pp = (OpenFlowMsg *)object; (void)pp;
    switch (field) {
        case 0: return long2string(pp->getOFtype());
        case 1: return long2string(pp->getAppType());
        case 2: return oppstring2string(pp->getNodeName());
        case 3: return oppstring2string(pp->getSourceAddress());
        case 4: return long2string(pp->getSourcePort());
        case 5: return oppstring2string(pp->getDestAddress());
        case 6: return long2string(pp->getDestPort());
        case 7: return double2string(pp->getPosX());
        case 8: return double2string(pp->getPosY());
        case 9: return double2string(pp->getPosZ());
        case 10: {std::stringstream out; out << pp->getInfo(); return out.str();}
        case 11: {std::stringstream out; out << pp->getRule(); return out.str();}
        default: return "";
    }
}

bool OpenFlowMsgDescriptor::setFieldValueAsString(void *object, int field, int i, const char *value) const
{
    omnetpp::cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount())
            return basedesc->setFieldValueAsString(object,field,i,value);
        field -= basedesc->getFieldCount();
    }
    OpenFlowMsg *pp = (OpenFlowMsg *)object; (void)pp;
    switch (field) {
        case 0: pp->setOFtype(string2long(value)); return true;
        case 1: pp->setAppType(string2long(value)); return true;
        case 2: pp->setNodeName((value)); return true;
        case 3: pp->setSourceAddress((value)); return true;
        case 4: pp->setSourcePort(string2long(value)); return true;
        case 5: pp->setDestAddress((value)); return true;
        case 6: pp->setDestPort(string2long(value)); return true;
        case 7: pp->setPosX(string2double(value)); return true;
        case 8: pp->setPosY(string2double(value)); return true;
        case 9: pp->setPosZ(string2double(value)); return true;
        default: return false;
    }
}

const char *OpenFlowMsgDescriptor::getFieldStructName(int field) const
{
    omnetpp::cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount())
            return basedesc->getFieldStructName(field);
        field -= basedesc->getFieldCount();
    }
    switch (field) {
        case 10: return omnetpp::opp_typename(typeid(localControllerInformations));
        case 11: return omnetpp::opp_typename(typeid(ofp_flow_table_header_fields));
        default: return nullptr;
    };
}

void *OpenFlowMsgDescriptor::getFieldStructValuePointer(void *object, int field, int i) const
{
    omnetpp::cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount())
            return basedesc->getFieldStructValuePointer(object, field, i);
        field -= basedesc->getFieldCount();
    }
    OpenFlowMsg *pp = (OpenFlowMsg *)object; (void)pp;
    switch (field) {
        case 10: return (void *)(&pp->getInfo()); break;
        case 11: return (void *)(&pp->getRule()); break;
        default: return nullptr;
    }
}


