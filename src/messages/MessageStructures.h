#ifndef MESSAGESTRUCTURES_H
#define MESSAGESTRUCTURES_H

#include <unordered_map>
#include "structures/CarInformations.h"

using namespace std;

enum MsgTypes{
    LC_INITIAL_STATUS           =   1,
    CC_RESPONSE_INITIAL_STATUS  =   2,
    VEHICLE_INITIAL_CC_REQUEST  =   3,
    CC_RESPONSE_VEHICLE_INITIAL =   4,
    VEHICLE_STATUS_POSITION     =   5,
    NETWORK_VIEW_STATUS         =   6,
    RTP_VIDEO_REQUEST           =   7,
    OF_REQUEST_LC_ADDR          =   8,
    OF_RESPONSE_LC_ADDR         =   9,
    LC_CHOICE_BY_CAR            =   10,
    FLOWTABLE_QOS_REQUEST       =   11,
    FLOWTABLE_QOS_REPLY         =   12,
    STATUS_QOS_REQUEST          =   13,
    STATUS_QOS_REPLY            =   14,
    STATUS_TABLE_MISS           =   15,
    STATUS_GET_QOS_REQUEST      =   16,
    STATUS_GET_QOS_REPLY        =   17,
    APP_REQUEST                 =   18,
    QOS_VIDEO                   =   19,
    QOS_APP                     =   20,
    LC_ADDRESS_DATA             =   21,
    CAR_POSITION_DATA           =   22,
    LC_LOCALNETVIEW_DATA        =   23,
    LC_POSITIONS_TO_UDPAPP      =   24,
    CC_LOCALCONTROLLER_CHANGE   =   25,
};

enum AppTypes{
  VIDEO     =       16,
  FTP       =       17,
  VOICE     =       18,
  WWW       =       19
};

enum PrioritiesTypes{
  SEVEN     =       7,
  SIX       =       6,
  FIVE      =       5,
  FOUR      =       4,
  THREE     =       3,
  TWO       =       2,
  ONE       =       1,
  NOTHING   =       0
};

struct localControllerInformations{
   std::vector<int> id;
   std::vector<string> address;
   std::vector<int> port;
   std::vector<int> portTCP;
   std::vector<double> posX;
   std::vector<double> posY;
   std::vector<double> posZ;
};

struct LCnetworkView{
    unordered_map< int, CarInformations> netView;
};

#endif
