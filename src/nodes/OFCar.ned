package nodes;

import inet.networklayer.configurator.ipv4.HostAutoConfigurator;

//standard
import inet.transportlayer.contract.ITCP;
import inet.applications.contract.ITCPApp;
import inet.applications.contract.IPingApp;
import inet.applications.contract.IUDPApp;
import inet.transportlayer.contract.IUDP;

//base
import inet.common.lifecycle.NodeStatus;
import inet.common.packet.PcapRecorder;
import inet.linklayer.contract.IWirelessNic;
import inet.linklayer.loopback.LoopbackInterface;
import inet.mobility.contract.IMobility;
import inet.networklayer.common.InterfaceTable;
import inet.networklayer.contract.INetworkLayer;
import inet.networklayer.contract.IRoutingTable;

//App Layer
import applications.ApplicationLayers.Car_AL;
import applications.AppControl;
import structures.interfaces.IUDPInterface;
import structures.interfaces.ITCPInterface;

//RTP
import inet.applications.rtpapp.RTPApplication;
import inet.transportlayer.rtp.RTCP;
import inet.transportlayer.rtp.RTP;
import ned.IdealChannel;

import structures.interfaces.RTPcarInterface;

module OFCar
{
    parameters:
        networkLayer.configurator.networkConfiguratorModule = "";

	    //standard	    
        @networkNode;
        @labels(wireless-node);
        @display("i=veins/node/car;is=vs");
        int numTcpApps = default(0);
        int numUdpApps = default(0);
        int numPingApps = default(0);
        bool hasTcp = default(numTcpApps > 0);
        bool hasUdp = default(numUdpApps > 0);
        string tcpType = default(firstAvailableOrEmpty("TCP", "TCP_lwIP", "TCP_NSC"));
        string udpType = default(firstAvailableOrEmpty("UDP"));
        networkLayer.proxyARP = default(false);

        //base
        bool hasStatus = default(false);
        int numRadios = default(1);
        int numPcapRecorders = default(0);
        string osgModel = default(""); // 3D model for OSG visualization, no 3D model by default
        string osgModelColor = default(""); // tint color, no colorization by default
        string mobilityType = default(numRadios > 0 ? "StationaryMobility" : "");
        string networkLayerType = default("IPv4NetworkLayer");
        string routingTableType = default("IPv4RoutingTable");
        bool forwarding = default(true);
        bool multicastForwarding = default(false);
        routingTable.forwarding = forwarding;
        routingTable.multicastForwarding = multicastForwarding;   // for IPv4, IPv6, Generic
        *.interfaceTableModule = default(absPath(".interfaceTable"));
        *.routingTableModule = default(routingTableType != "" ? absPath(".routingTable") : "");
        *.mobilityModule = default(mobilityType != "" ? absPath(".mobility") : "");

        //RTPHost
        string profileName;
        string destinationAddress;
        int portNumber;
        double bandwidth;
        string fileName;
        int payloadType;
        //hasUdp = true;      

    gates:
        input radioIn[numRadios] @directIn;
    submodules:
        //base
        status: NodeStatus if hasStatus {
            @display("p=50,50");
        }

        mobility: <mobilityType> like IMobility if mobilityType != "" {
            parameters:
                @display("p=50,411");
        }

        networkLayer: <networkLayerType> like INetworkLayer {
            parameters:
                @display("p=286,315;q=queue");
        }

        routingTable: <routingTableType> like IRoutingTable if routingTableType != "" {
            parameters:
                @display("p=50,123;is=s");
        }

        interfaceTable: InterfaceTable {
            parameters:
                @display("p=50,186;is=s");
        }

        pcapRecorder[numPcapRecorders]: PcapRecorder {
            @display("p=50,271,r,10");
        }

        lo0: LoopbackInterface {
            @display("p=363,442");
        }

        wlan[numRadios]: <default("Ieee80211Nic")> like IWirelessNic {
            parameters:
                @display("p=204,442,row,60;q=queue");
        }

        AppLayer: Car_AL {
            @display("p=275,42");
        }

        rtpcarinterface: RTPcarInterface {
            @display("p=503,137");
        }

        //standard
        ac_wlan: HostAutoConfigurator {
            @display("p=50,346");
        }

        tcpApp[numTcpApps]: <> like ITCPInterface {
            parameters:
                @display("p=204,115,row,60");
        }


        tcp: <tcpType> like ITCP if hasTcp {
            parameters:
                @display("p=204,207");
        }

        udpApp[numUdpApps]: <> like IUDPInterface {
            parameters:
                @display("p=348,115,row,60");
        }

        udp: <udpType> like IUDP if hasUdp {
            parameters:
                @display("p=348,207");
        }

        pingApp[numPingApps]: <default("PingApp")> like IPingApp {
            parameters:
                @display("p=526,315,row,60");
        }



        //RTP
        rtpApp: RTPApplication {
            parameters:
                profileName = profileName;
                destinationAddress = destinationAddress;
                portNumber = portNumber;
                bandwidth = bandwidth;
                fileName = fileName;
                payloadType = payloadType;
                @display("p=503,58");
        }
        rtp: RTP {
            @display("p=590,58");
        }
        rtcp: RTCP {
            @display("p=590,168");
        }
    connections allowunconnected:
        //standard       

        for i=0..numTcpApps-1 {
            tcpApp[i].tcpOut --> tcp.appIn++;
            tcpApp[i].tcpIn <-- tcp.appOut++;
            tcpApp[i].upperTCPAppOut --> AppLayer.tcpdataIn;
            tcpApp[i].upperTCPAppIn <-- AppLayer.tcpdataOut;
        }

        tcp.ipOut --> networkLayer.transportIn++ if hasTcp;
        tcp.ipIn <-- networkLayer.transportOut++ if hasTcp;

        for i=0..numUdpApps-1 {
            udpApp[i].udpOut --> udp.appIn++;
            udpApp[i].udpIn <-- udp.appOut++;            
            udpApp[i].upperAppOut --> AppLayer.udpdataIn;
            udpApp[i].upperAppIn <-- AppLayer.udpdataOut;
        }

        udp.ipOut --> networkLayer.transportIn++ if hasUdp;
        udp.ipIn <-- networkLayer.transportOut++ if hasUdp;

        for i=0..numPingApps-1 {
            networkLayer.pingOut++ --> pingApp[i].pingIn;
            networkLayer.pingIn++ <-- pingApp[i].pingOut;
        }

        //base

        networkLayer.ifOut++ --> lo0.upperLayerIn;
        lo0.upperLayerOut --> networkLayer.ifIn++;


        for i=0..sizeof(radioIn)-1 {
            radioIn[i] --> { @display("m=s"); } --> wlan[i].radioIn;
            wlan[i].upperLayerOut --> networkLayer.ifIn++;
            wlan[i].upperLayerIn <-- networkLayer.ifOut++;
        }

        //RTP Connections
        // transport connections
        rtpApp.rtpOut --> rtp.appIn;
        rtpApp.rtpIn <-- rtp.appOut;

        rtp.udpOut --> rtpcarinterface.upperRTPIn;
        rtp.udpIn <-- rtpcarinterface.upperRTPOut;
        rtcp.udpOut --> rtpcarinterface.upperRTCPIn;
        rtcp.udpIn <-- rtpcarinterface.upperRTCPOut;

        rtpcarinterface.downUDPOut --> udp.appIn++;
        rtpcarinterface.downUDPIn <-- udp.appOut++;

        rtp.rtcpOut --> IdealChannel --> rtcp.rtpIn;
        rtcp.rtpOut --> IdealChannel --> rtp.rtcpIn;
        
        AppLayer.rtpInterfaceOut --> rtpcarinterface.appControlIn;
        rtpcarinterface.appControlOut --> AppLayer.rtpInterfaceIn;
}
