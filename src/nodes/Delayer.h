#ifndef __DELAYER_H
#define __DELAYER_H

#include <string.h>
#include <omnetpp.h>

using namespace omnetpp;


class Delayer : public cSimpleModule
{

protected:
    double delay;
protected:
    virtual void initialize() override;
    virtual void handleMessage(cMessage *msg) override;

protected:
    virtual void delaySendD1(cMessage *msg);
    virtual void delaySendD2(cMessage *msg);

};

#endif
