#ifndef __RSU_H
#define __RSU_H

#include "inet/common/INETDefs.h"
#include "inet/networklayer/contract/ipv4/IPv4Address.h"
#include "inet/common/lifecycle/ILifecycle.h"
#include <string>
#include <sstream>
#include <iostream>

class RSU : public cSimpleModule
{
    protected:
        virtual void handleMessage(cMessage *msg) override;
    public:
        RSU() {}

};

#endif
