#include "Delayer.h"


Define_Module(Delayer);

void Delayer::initialize()
{
    delay = par("delay");
}

void Delayer::handleMessage(cMessage *msg)
{

    std::string gate = msg->getArrivalGate()->getBaseName();

    std::cout << "TESTE: " << gate << std::endl;

    if(gate.compare("A") == 0){
        send(msg, "B");
        scheduleAt(simTime()+delay, msg);
    }
    else if(gate.compare("B") == 0){
        send(msg, "A");
        scheduleAt(simTime()+delay, msg);
    }
}

void Delayer::delaySendD1(cMessage *msg)
{

}

void Delayer::delaySendD2(cMessage *msg)
{

}
