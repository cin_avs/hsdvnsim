/*
 * VirtualCar.h
 *
 *  Created on: 13 de dez de 2017
 *      Author: alex
 */

#ifndef VirtualCar_H
#define VirtualCar_H


#include <string>
#include <sstream>
#include <list>
#include <cstring>
#include <iostream>
#include <fstream>
#include <unordered_map>
#include <vector>
#include <cstdlib>
#include <cmath>

using namespace std;

class VirtualCar{
    private:
        int idCar;
        int idCluster;
        string name;
        int total_values;
        string nodeSourceAddress;
        double position_x, position_y, position_z;
        vector<double> positions;
    protected:
/*
    public:
        //~VirtualCar();
        virtual int getID();
        virtual void setIdCar(int id);
        virtual int getTotalValues();
        virtual int getClusterId();
        virtual void setCluster(int id_Cluster);
        virtual void setIdCluster(int id);
        virtual vector<double> getPosition();
        virtual double getValue(int index);
        virtual void setPosition(double value);
        virtual string getName();
        virtual double getPosition_x();
        virtual void setPosition_x(double x);
        virtual double getPosition_y();
        virtual void setPosition_y(double y);
        virtual double getPosition_z();
        virtual void setPosition_z(double z);
*/
    public:

        VirtualCar(int idCar, vector<double> &positions, string name = ""){
            this->idCar = idCar;
            total_values = positions.size();

            for(int i = 0; i < total_values; i++){
                this->positions.push_back(positions[i]);
            }

            this->name = name;
            idCluster = -1;
        }

        VirtualCar(int id, string address, double x, double y, double z){
            this->idCar = id;
            this->nodeSourceAddress = address;
            this->position_x = x;
            this->position_y = y;
            this->position_z = z;
        }

        VirtualCar(double x, double y, double z, int id){
            this->position_x = x;
            this->position_y = y;
            this->position_z = z;
            this->idCar = id;
            this->idCluster = 0;
        }

        VirtualCar (string fromToString){
            int size = fromToString.size() + 1;
            char str[size];
            char * pch;
            string aux;
            list<string> all_values;
            int counter = 0;
            strcpy(str, fromToString.c_str());
            pch = strtok(str, " ");
            while (pch != NULL){
                istringstream(pch) >> aux;
                all_values.push_back(aux);
                pch = strtok(NULL, " ");
                if (counter == 0){
                    this->idCar = atoi(aux.c_str());
                }
                if(counter == 1){
                    this->nodeSourceAddress = atoi(aux.c_str());
                }
                if (counter == 2){
                    this->position_x = atof(aux.c_str());
                }
                if (counter == 3){
                    this->position_y = atof(aux.c_str());
                }
                if (counter == 4){
                    this->position_z = atof(aux.c_str());
                }
                if (counter == 5){
                    this->idCluster = atoi(aux.c_str());
                }
                counter = counter + 1;
            }
            toString();
        }

        VirtualCar(){
            this->idCar = 0;
            this->nodeSourceAddress = "";
            this->idCluster = 0;
            this->name = "";
            this->total_values = 0;
            //this->positions = "";
        }

        string printPosition(){
            ostringstream oss;
            oss << idCar << " " << position_x << " " << position_y << " " << position_z << " " << idCluster;
            string str = oss.str().c_str();
            // printf("%s\n", str.c_str());
            return str;
        }

        double calculateDistance(VirtualCar &another){
            double distance;

            //cout << "=====================================================" << std::endl;
            //cout << "Distancia another x: " << another.getX() << std::endl;
            //cout << "Distancia another y: " << another.getY() << std::endl;
            //cout << "Distancia car x: " << position_x << std::endl;
            //cout << "Distancia car y: " << position_y << std::endl;
            distance = sqrt(pow(another.getX() - this->position_x,  2) + pow(another.getY() - this->position_y , 2));
            //printPosition();
            //another.printPosition();
            // printf("DISTANCE %f\n", distance);
            return distance;
        }


            int getID(){
                return idCar;
            }

            void setIdCar(int id){
                idCar = id;
            }

            void setAddressCar(string address){
                nodeSourceAddress = address;
            }

            string getAddressCar(){
                return nodeSourceAddress;
            }

            int getTotalValues(){
                return total_values;
            }

            int getClusterId(){
                return idCluster;
            }

            void setCluster(int id_Cluster){
                idCluster = id_Cluster;
            }

            void setIdCluster(int id){
                idCluster = id;
            }

            vector<double> getPosition(){
                return positions;
            }

            double getValue(int index){
                return positions[index];
            }

            void setPosition(double value){
                positions.push_back(value);
            }

            string getName(){
                return name;
            }

            double getX(){
                return position_x;
            }

            void setX(double x){
                position_x = x;
            }

            double getY(){
                return position_y;
            }

            void setY(double y){
                position_y = y;
            }

            double getZ(){
                return position_z;
            }

            void setZ(double z){
                position_z = z;
            }


            string toString(){
                ostringstream oss;
                oss << getID() << " " << getAddressCar() << " " << getX() << " " << getY() << " " << getZ() << " " << getClusterId();
                //printf("%s\n", oss.str().c_str());
                return oss.str().c_str();
            }


};

#endif
