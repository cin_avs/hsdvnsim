#ifndef __CARPOSITION_H
#define __CARPOSITION_H

#include <string>
#include <sstream>
#include <iostream>
#include <cmath>

using namespace std;

class CarInformations
{
    public:
        int idCar;
        string nameCar;
        string addressCar;
        int udpPortConn;
        int timeAdded;
        string localControllerAddressChoice;
        int localControllerPortChoice;
        double x;
        double y;
        double z;

    public:
        virtual string printInformations();
        virtual int getIdCar();
        virtual string getNameCar();
        virtual string getAddressCar();
        virtual double getPosX();
        virtual double getPosY();
        virtual double getPosZ();
        virtual int getTime();
};

#endif
