#ifndef __GENERALCONTROLLER_H
#define __GENERALCONTROLLER_H

namespace inet {

class GeneralController
{
    public:
        //Priority Levels based IEEE 802.1d
        enum UserPriority {
            N0 = 0, // Level 0
            N1 = 1, // Level 1
            N2 = 2, // Level 2
            N3 = 3, // Level 3
            N4 = 4, // Level 4
            N5 = 5, // Level 5
            N6 = 6, // Level 6
            N7 = 7  // Level 7
        };
    protected:
        virtual void creatingNetworkView();
        virtual void creatingClusters();
    public:
        GeneralController() {}
        virtual ~GeneralController();
};

}

#endif
