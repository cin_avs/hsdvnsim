#include <stdio.h>
#include <string.h>
#include <omnetpp.h>
#include <unordered_map>
#include "structures/openflow.h"
#include "structures/FlowTable.h"

using namespace omnetpp;


void FlowTable::addFlowEntry(int id, Table t){
    flowTables.emplace(id, t);
}


bool FlowTable::delFlowEntry(int id){

    bool value = false;
    if(findFlowEntry(id)){
        cout << "Flow table not found!";
    }else{
        flowTables.erase(id);
        value = true;
    }
    return value;

}


bool FlowTable::findFlowEntry(int id){
    bool b = false;
    if(flowTables.find(id) == flowTables.end()){ // If key not found in map iterator to end is returned
        cout << "Flow not found \n\n";
    }else{
        b = true;
    }
    return b;
}

Table FlowTable::getTable(int id){

    auto search = flowTables.find(id);
    if (search != flowTables.end()) {
        ret = search->second;
    } else {
        std::cout << "TABLE Not found\n";
    }

    return ret;

}

void FlowTable::printFlowEntry(){

    for(auto elem : flowTables){
        Table table = elem.second;
        table.printTableEntry();
    }
}
