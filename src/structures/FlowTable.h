#ifndef FlowTable_H
#define FlowTable_H

#include <string.h>
#include <omnetpp.h>
#include <unordered_map>
#include "structures/openflow.h"
#include "structures/Table.h"

using namespace omnetpp;

class FlowTable{
    protected:
        //ofp_flow_table_header_fields match;
        Table ret;
        unordered_map<int, Table> flowTables;
    public:
        virtual void addFlowEntry(int id, Table t);
        virtual bool delFlowEntry(int id);
        virtual bool findFlowEntry(int id);
        virtual void printFlowEntry();
        virtual Table getTable(int id);
};

#endif
