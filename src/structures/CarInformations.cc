#include "CarInformations.h"

string CarInformations::printInformations(){
    ostringstream oss;
    oss << "CAR: " << nameCar <<" Address=(" << addressCar << ") Positions= [" << x << ", " << y << ", " << z << "]" ;
    string str = oss.str().c_str();
    return str;
}

int CarInformations::getIdCar()
{
    return idCar;
}

string CarInformations::getNameCar()
{
    return nameCar;
}

string CarInformations::getAddressCar()
{
    return addressCar;
}

double CarInformations::getPosX()
{
    return x;
}

double CarInformations::getPosY()
{
    return y;
}

double CarInformations::getPosZ()
{
    return z;
}

int CarInformations::getTime(){
    return timeAdded;
}

