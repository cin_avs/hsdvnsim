#ifndef __LOCALNETVIEW_H
#define __LOCALNETVIEW_H

#include <string>
#include <sstream>
#include <iostream>
#include <cmath>
#include <unordered_map>
#include "structures/CarInformations.h"

using namespace std;

class LocalNetView
{
    protected:
        int idLocalView;
        string nameLocalController;
        unordered_map< int, CarInformations> netView;
    public:
        unordered_map< int, CarInformations> getNetView(){
            return netView;
        }

        void setNetView(unordered_map< int, CarInformations> &someMap){
            netView = someMap;
        }

};

#endif
