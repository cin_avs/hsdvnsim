#ifndef OPENFLOW_H
#define OPENFLOW_H

using namespace std;
//using namespace omnetpp;


#ifdef SWIG
#define OFP_ASSERT(EXPR) /*SWIG can't handle OFP_ASSERT.*/
#elif !defined(__cplusplus)
#define OFP_ASSERT(EXPR)
        extern int (*build_assert(void))[ sizeof(struct {
                     unsigned int build_assert_failed : (EXPR) ? 1 : -1; })]
#else /* __cplusplus */
#define OFP_ASSERT(_EXPR) typedef int build_assert_failed[(_EXPR) ? 1 : -1]
#endif

/* Version number: * OpenFlow versions released: 0x01 = 1.0 ; 0x02 = 1.1 ; 0x03 = 1.2 * 0x04 = 1.3.X ; 0x05 = 1.4.X ; 0x06 = 1.5.X */
/* The most significant bit in the version field is reserved and must * be set to zero. */

#define OFP_VERSION 0x11 //version 2.0 in this work
#define OFP_MAX_TABLE_NAME_LEN 32
#define OFP_MAX_PORT_NAME_LEN 16

//Official IANA registered port for Openflow

#define OFP_TCP_PORT 6653
#define OFP_SSL_PORT 6653

#define OFP_ETH_ALEN 6 //Bytes in an Ethernet Address

// Port numering. Ports are numbered starting from 1.
enum ofp_port_num{
    /* Maximum number of physical and logical switch ports. */
    OFPP_MAX    = 0xffffff00,
    /* Reserved OpenFlow Port (fake output "ports"). */
    OFPP_UNSET   = 0xfffffff7, /* Output port not set in action-set. used only in OXM_OF_ACTSET_OUTPUT. */
    OFPP_IN_PORT = 0xfffffff8, /* Send the packet out the input port. This reserved port must be explicitly used in order to send back out of the input port. */
    OFPP_TABLE   = 0xfffffff9, /* Submit the packet to the first flow table NB: This destination port can only be used in packet-out messages. */
    OFPP_NORMAL = 0xfffffffa, /* Forward using non-OpenFlow pipeline. */
    OFPP_FLOOD   = 0xfffffffb, /* Flood using non-OpenFlow pipeline. */
    OFPP_ALL    = 0xfffffffc, /* All standard ports except input port. */
    OFPP_CONTROLLER = 0xfffffffd, /* Send to controller. */
    OFPP_LOCAL   = 0xfffffffe, /* Local openflow "port". */
    OFPP_ANY   = 0xffffffff /* Special value used in some requests when no port is specified (i.e. wildcarded). */
};

//Type of openflow messages
enum ofp_type{
    /*Immutable messages.*/
    OFPT_HELLO              =   0,
    OFPT_ERROR              =   1,
    OFPT_ECHO_REQUEST       =   2,
    OFPT_ECHO_REPLY         =   3,
    OFPT_ECHO_EXPERIMENTER  =   4,

    /* Switch configuration messages. */
    OFPT_QOS_REQUEST   =   5,
    OFPT_QOS_REPLY     =   6,
    OFPT_GET_CONFIG_REQUEST =   7,
    OFPT_GET_CONFIG_REPLY   =   8,
    OFPT_SET_CONFIG         =   9,

    /* Asynchronous messages. */
    OFPT_PACKET_IN          =   10,
    OFPT_FLOW_REMOVED       =   11,
    OFPT_PORT_STATUS        =   12,

    /* Controller command messages. */
    OFPT_PACKET_OUT         =   13,
    OFPT_FLOW_MOD           =   14,
    OFPT_GROUP_MOD          =   15,
    OFPT_PORT_MOD           =   16,
    OFPT_TABLE_MOD          =   17,
    OFPT_TABLE_MISS         =   18,
    OFPT_TABLE_MISS_REPLY   =   19,

    /* Controller Status async message. */
    OFPT_CONTROLLER_STATUS  =   20,
};

/* Header on all Openflow Packets*/
enum ofp_table_types{
    OF_TABLE_QOS     =   21,
};



/*Structure to match in flow tables
 * obs:"This part is specific to version 1.5 openflow"
 * */
struct ofp_flow_table_header_fields{
  int sourcePort;
  string srcMAC;
  string dstMAC;
  int vlanId;
  int priority;
  int ethernetType; /*ofp_flow_table_ethernet_type*/
  string srcIP;
  string dstIP;
  string ipProto;
  int srcTcpPort;
  int dstTcpPort;
  int action;
  int counter;
};


/* ## ----------------- ## */
/* ## OpenFlow Actions. ## */
/* ## ----------------- ## */



enum ofp_cluster_priority_action_type{
    OFPT_PRIORITY_SEVEN   =   7,
    OFPT_PRIORITY_SIX     =   6,
    OFPT_PRIORITY_FIVE    =   5,
    OFPT_PRIORITY_FOUR    =   4,
    OFPT_PRIORITY_THREE   =   3,
    OFPT_PRIORITY_TWO     =   2,
    OFPT_PRIORITY_ONE     =   1,
    OFPT_PRIORITY_NOTHING =   0
};

enum ofp_app_types{
    OF_TABLE_MISS     =   30,
    OF_VIDEO    =   31,
    OF_FTP      =   32,
    OF_WWW      =   33,
    OF_VOIP     =   34,
};


#endif





