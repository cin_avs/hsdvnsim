#include "LCinformations.h"

string LCinformations::printInformations()
{
    ostringstream oss;
    oss << "Address=(" << address << ") Positions= " << x << ", " << y << ", " << z ;
    string str = oss.str().c_str();
    return str;
}

string LCinformations::returnPositions()
{
    ostringstream oss;
    oss << address << "|" << x << "|" << y << "|" << z ;
    string str = oss.str().c_str();
    return str;
}

int LCinformations::getID_LC(){
    return idLC;
}

string LCinformations::getAddress_LC(){
    return address;
}

int LCinformations::getPort_LC(){
    return port;
}

int LCinformations::getPortTCP_LC(){
    return portTCP;
}

double LCinformations::getPOSX_LC(){
    return x;
}

double LCinformations::getPOSY_LC(){
    return y;
}

double LCinformations::getPOSZ_LC(){
    return z;
}

