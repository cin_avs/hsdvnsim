#ifndef __LCINFORMATIONS_H
#define __LCINFORMATIONS_H

#include <string>
#include <sstream>
#include <iostream>
#include <cmath>

using namespace std;

class LCinformations
{
    public:
        int idLC;
        string address;
        int port;
        int portTCP;
        double x;
        double y;
        double z;

    public:
        virtual string printInformations();
        virtual string returnPositions();
        virtual int getID_LC();
        virtual string getAddress_LC();
        virtual int getPort_LC();
        virtual int getPortTCP_LC();
        virtual double getPOSX_LC();
        virtual double getPOSY_LC();
        virtual double getPOSZ_LC();
};

#endif
