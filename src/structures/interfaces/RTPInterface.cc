#include "RTPInterface.h"

#include "inet/common/ModuleAccess.h"
#include "inet/networklayer/common/L3AddressResolver.h"
#include "inet/transportlayer/contract/udp/UDPControlInfo_m.h"
#include "messages/MessageStructures.h"
#include "messages/StatusMsg_m.h"
#include "messages/VideoRTPMessage_m.h"
#include <iomanip>
#include <iostream>
#include "inet/transportlayer/udp/UDPPacket.h"

namespace inet {

//namespace rtp {

Define_Module(RTPInterface);

simsignal_t RTPInterface::rcvdPkSignal = registerSignal("rcvdPk");
simsignal_t RTPInterface::sentPkSignal = registerSignal("sentPk");

void RTPInterface::initialize(int stage)
{
    if (stage == INITSTAGE_LOCAL) {
        WATCH(bytesRcvd);
        WATCH(bytesSent);
    }
}

void RTPInterface::handleMessage(cMessage *msg)
{
    std::string gate = msg->getArrivalGate()->getBaseName();
    std::string verify = msg->getFullName();

    if(gate.compare("downUDPIn") == 0){
        saveInfos(msg);
        //cPacket *rcv = PK(msg);
        //emit(rcvdPkSignal, rcv); //estatística pacotes recebidos
        delete(msg);
    }else if(gate.compare("upperRTPIn") == 0 || gate.compare("upperRTCPIn") == 0){
        if(verify.compare("RTPPacket") == 0 || verify.compare("RTCPCompoundPacket") == 0){
            sendPacketData(msg);
        }else{
            send(msg, "downUDPOut");
        }
    }else{
        send(msg, "downUDPOut");
    }

}

//VIDEO ENVIADO PARA OS VEÍCULOS

void RTPInterface::saveInfos(cMessage *msg)
{
    std::string verify = msg->getClassName();
    std::string ver = msg->getFullName();

    if(verify.compare("StatusMsg") == 0){
        if(ver.compare("SETQOSPARAM") == 0){
            StatusMsg *status = check_and_cast<StatusMsg *>(msg);
            destaddresses.push_back(status->getSourceAddress());
            destPorts.push_back(status->getSourcePort());
            qos.push_back(status->getQosAction());
        }
    }else{
        //TODO do nothing
    }
}


void RTPInterface::sendPacketData(cMessage *msg)
{
    socket.setOutputGate(gate("downUDPOut"));

    cPacket *packet;

    if(PK(msg)){
        packet = PK(msg);
    }

    UDPSendCommand *ctrl = check_and_cast<UDPSendCommand *>(packet->removeControlInfo());

    for(int i=0; i < destaddresses.size();i++){
        const char *addr = destaddresses.at(i).c_str();
        L3Address nodeAddrTeste = L3AddressResolver().resolve(addr);
        VideoRTPMessage *pkt = new VideoRTPMessage("VideoFrame");


        /*
        if(timeTest == 0.0){
            timeTest = simTime().dbl();
            pkt->setQosParameter(qos.at(i));
            priorCountVector.record(qos.at(i));
        }
        else if(simTime().dbl() - timeTest > fRand(1.0, 5.0)){
            int randQos = (int)fRand(3.0, 6.0);
            pkt->setQosParameter(randQos);
            priorCountVector.record(randQos);
            timeTest = 0.0;
        }else{
            priorCountVector.record(qos.at(i));
        }

        */

        pkt->setQosParameter(0);

        pkt->setSendTime(simTime());
        pkt->setByteLength(packet->getByteLength());


        ctrl->setDestAddr(nodeAddrTeste); //seta as informações do pacote
        ctrl->setDestPort(destPorts.at(i));
        ctrl->setSockId(socket.getSocketId());
        pkt->setControlInfo(ctrl);

        //std::cout << "SENDING TO: " << destaddresses.at(i).c_str() << std::endl;

        send(pkt, "downUDPOut");

        //socket.sendTo(pkt, nodeAddrTeste, destPorts.at(i));
    }

    /*
     * TODO Make a method who insert into an unordered_map the packet's data to transmit to another delayed customers
     * who sent requests.
     */

}

double RTPInterface::fRand(double fMin, double fMax)
{
    double f = (double)rand() / RAND_MAX;
    return fMin + f * (fMax - fMin);
}


void RTPInterface::finish()
{
    recordScalar("bytesRcvd", bytesRcvd);
    recordScalar("bytesSent", bytesSent);

    delayCountStats.recordAs("DELAYcount");

}

}


