#include "RTPcarInterface.h"

#include "inet/common/ModuleAccess.h"
#include "messages/StatusMsg_m.h"
#include "messages/VideoRTPMessage_m.h"

namespace inet {

Define_Module(RTPcarInterface);

simsignal_t RTPcarInterface::rcvdPkSignal = registerSignal("rcvdPk");

void RTPcarInterface::initialize(){/*TODO*/}

void RTPcarInterface::handleMessage(cMessage *msg)
{
    //cModule *host = nullptr;
    //host = getContainingNode(this);
    std::string gate = msg->getArrivalGate()->getBaseName();

    std::string verify (msg->getClassName());

    if(verify.compare("StatusMsg") == 0 && gate.compare("appControlIn") == 0){ //recebe parametro de qos da camada de aplicação do carro
        StatusMsg *status = check_and_cast<StatusMsg *>(msg);
        qos = status->getQosAction();
    }

    //std::cout << "PACKET ARRIVED ON GATE: " << gate << std::endl;

    if(gate.compare("appControlIn") == 0){
        cPacket *pkt = check_and_cast<cPacket *>(msg);

        if(verify.compare("VideoRTPMessage") == 0){
            VideoRTPMessage *pkt = check_and_cast<VideoRTPMessage *>(msg);
            simtime_t sendTime = pkt->getSendTime();
            double delay = simTime().dbl() - sendTime.dbl();

            //std::cout << "======================="<< std::endl;
            //std::cout << "TEMPO DE ENVIO DO FRAME: " << sendTime.dbl() << std::endl;
            //std::cout << "TEMPO DE CHEGADA DO FRAME: " << simTime().dbl() << std::endl;
            //std::cout << "======================="<< std::endl;

            //simtime_t delay = simTime() - pkt->getSendTime();
            delayCountVector.record(delay);
            delayCountStats.collect(delay);
        }

        emit(rcvdPkSignal, pkt); //estatística
        delete(msg);
    }
    else if(gate.compare("upperRTPIn") == 0 || gate.compare("upperRTCPIn") == 0){
        if(verify.compare("omnetpp::cPacket") == 0 || verify.compare("inet::rtp::RTPPacket") == 0){

            //std::cout << "TESTE TEMPO DE ENVIO: " << simTime() << std::endl;

            VideoRTPMessage *pkt = new VideoRTPMessage("VideoFrame");

            if(timeTest == 0.0){
                timeTest = simTime().dbl();
                pkt->setQosParameter(qos);
                priorCountVector.record(qos);
            }
            else if(simTime().dbl() - timeTest > fRand(0.0, 7.0)){
                int randQos = (int)fRand(3.0, 6.0);
                pkt->setQosParameter(randQos);
                priorCountVector.record(randQos);
                timeTest = 0.0;
            }else{
                priorCountVector.record(qos);
            }

            //pkt->setQosParameter(3);
            //pkt->setQosParameter(qos);

            pkt->setByteLength(PK(msg)->getByteLength()); //Não sei se irá setar o tamanho do pacote corretamente
            pkt->setSendTime(simTime());
            cPacket *packet = PK(msg);
            UDPSendCommand *ctrl = check_and_cast<UDPSendCommand *>(packet->getControlInfo());
            pkt->setControlInfo(ctrl);
            send(pkt, "downUDPOut");
            //send(msg, "downUDPOut");
        }else{
            send(msg, "downUDPOut");
        }
    }
    else{
        send(msg, "downUDPOut");
    }
}

double RTPcarInterface::fRand(double fMin, double fMax)
{
    double f = (double)rand() / RAND_MAX;
    return fMin + f * (fMax - fMin);
}

void RTPcarInterface::finish()
{
    //EV_INFO << getFullPath() << ": received " << numReceived << " packets\n";

    delayCountStats.recordAs("PositionsCount");
    delayCountVector.isName("DelayHandover");
}

}


