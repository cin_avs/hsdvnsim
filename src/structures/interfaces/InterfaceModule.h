#ifndef __INTERFACEMODULE_H
#define __INTERFACEMODULE_H

#include <string.h>
#include <omnetpp.h>

using namespace omnetpp;

class InterfaceModule : public cSimpleModule
{
    protected:
        virtual void initialize() override;
        virtual void handleMessage(cMessage *msg) override;

};

#endif
