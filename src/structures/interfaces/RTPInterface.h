#ifndef __RTPINTERFACE_H
#define __RTPINTERFACE_H

#include <string.h>
#include <omnetpp.h>
#include "inet/common/INETDefs.h"
#include "inet/transportlayer/contract/udp/UDPSocket.h"
#include <unordered_map>
#include "messages/VideoRTPMessage_m.h"


namespace inet {


class RTPInterface : public cSimpleModule
{
    protected:
        UDPSocket socket;
        std::vector<std::string> destaddresses;
        std::vector<int> destPorts;
        std::vector<int> qos;
        std::vector<VideoRTPMessage> videoData;

        //int qos;

        //std::unordered_map< std::string, std::string > qosMap;

        long bytesRcvd = 0;
        long bytesSent = 0;

        static simsignal_t rcvdPkSignal;
        static simsignal_t sentPkSignal;

        cLongHistogram delayCountStats;
        cOutVector delayCountVector;
        cOutVector priorCountVector;

        double timeTest = 0.0;

    protected:
        virtual void initialize(int stage) override;
        virtual void handleMessage(cMessage *msg) override;
        virtual void saveInfos(cMessage *msg);
        virtual void sendPacketData(cMessage *pkt);
        virtual double fRand(double fMin, double fMax);
        virtual void finish() override;
};

}

#endif

