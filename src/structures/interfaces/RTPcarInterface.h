#ifndef __RTPCARINTERFACE_H
#define __RTPCARINTERFACE_H

#include <string.h>
#include <omnetpp.h>
#include "inet/common/INETDefs.h"
#include "inet/applications/base/ApplicationBase.h"
#include "inet/transportlayer/rtp/RTP.h"
#include "inet/transportlayer/rtp/RTCPPacket1_m.h"


namespace inet {

class RTPcarInterface : public cSimpleModule
{
    protected:
        static simsignal_t rcvdPkSignal;
        int qos;
        double timeTest = 0.0;
        cLongHistogram delayCountStats;
        cOutVector delayCountVector;
        cOutVector priorCountVector;
    protected:
        virtual void initialize() override;
        virtual void handleMessage(cMessage *msg) override;
        virtual double fRand(double fMin, double fMax);
        virtual void finish() override;
};

}
//}

#endif

