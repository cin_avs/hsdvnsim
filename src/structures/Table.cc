#include "Table.h"

using namespace omnetpp;

void Table::addTableEntry(int id, ofp_flow_table_header_fields match){
    table.emplace(id, match);
}
bool Table::delTableEntry(int id,ofp_flow_table_header_fields m){
    bool verify = false;

    if(matchingTableEntry(m)){
        table.erase(id);
        verify = true;
    }

    return verify;
}

bool Table::findRule(int id){
    bool verify = false;

    auto search = table.find(id);
    if (search != table.end()) {
        verify = true;
    } else {
        std::cout << "TABLE Not found id: " << id << std::endl;
    }
    return verify;
}

ofp_flow_table_header_fields Table::getRule(int id)
{
    auto search = table.find(id);
    if (search != table.end()) {
        ret = search->second;
    } else {
        std::cout << "TABLE Not found\n";
    }
    return ret;
}

bool Table::matchingTableEntry(ofp_flow_table_header_fields match){
    bool verify;
    for(auto f : table){
        if(f.second.sourcePort == match.sourcePort &&
                f.second.srcMAC == match.srcMAC &&
                f.second.dstMAC == match.dstMAC &&
                f.second.vlanId == match.vlanId &&
                f.second.priority == match.priority &&
                f.second.ethernetType == match.ethernetType &&
                f.second.srcIP == match.srcIP &&
                f.second.dstIP == match.dstIP &&
                f.second.ipProto == match.ipProto &&
                f.second.srcTcpPort == match.srcTcpPort &&
                f.second.dstTcpPort == match.dstTcpPort &&
                f.second.action == match.action &&
                f.second.counter == match.counter){

             verify = true;
        }else{
            verify = false;
        }
    }
    return verify;
}

bool Table::verifyTableParameter(string param)
{
    stringstream p(param);
    int x = 0;
    p >> x;

    bool test;
    for(auto f : table){
        if(f.second.sourcePort == x ||
        f.second.srcMAC == param    ||
        f.second.dstMAC == param    ||
        f.second.vlanId == x        ||
        f.second.priority == x      ||
        f.second.ethernetType == x  ||
        f.second.srcIP  == param    ||
        f.second.dstIP  == param    ||
        f.second.ipProto == param   ||
        f.second.srcTcpPort == x    ||
        f.second.dstTcpPort == x    ||
        f.second.action ==  x       ||
        f.second.counter == x){

            test = true;
        }else{
            test = false;
        }
    }
    return test;
}


void Table::printTableEntry(){

    for(auto elem : table){
        /*
        EV << elem.first << " : " << elem.second.sourcePort << "|" << elem.second.srcMAC <<
                "|" << elem.second.dstMAC << "|" << elem.second.vlanId << "|" << elem.second.priority <<
                "|" << elem.second.ethernetType << "|" << elem.second.srcIP << "|" << elem.second.dstIP <<
                "|" << elem.second.ipProto << "|" << elem.second.srcTcpPort << "|" << elem.second.dstTcpPort <<
                "|" << elem.second.action << "|" << elem.second.counter << "\n";
        */
        std::cout << "Table ID: " << elem.first << "\n";
        std::cout << "Table Informations..." << "\n\n";
        std::cout << elem.second.sourcePort << "|" << elem.second.srcMAC <<
                "|" << elem.second.dstMAC << "|" << elem.second.vlanId << "|" << elem.second.priority <<
                "|" << elem.second.ethernetType << "|" << elem.second.srcIP << "|" << elem.second.dstIP <<
                "|" << elem.second.ipProto << "|" << elem.second.srcTcpPort << "|" << elem.second.dstTcpPort <<
                "|" << elem.second.action << "|" << elem.second.counter << "\n\n";
    }

}
