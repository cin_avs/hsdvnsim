#ifndef Table_H
#define Table_H

#include <string.h>
#include <omnetpp.h>
#include <unordered_map>
#include "structures/openflow.h"
#include "structures/Table.h"

class Table{
    protected:
        ofp_flow_table_header_fields ret;
        unordered_map<int, ofp_flow_table_header_fields> table;
    public:
        virtual void addTableEntry(int id, ofp_flow_table_header_fields match);
        virtual bool delTableEntry(int id, ofp_flow_table_header_fields m);
        virtual bool matchingTableEntry(ofp_flow_table_header_fields match);
        virtual bool verifyTableParameter(string param);
        virtual void printTableEntry();
        virtual ofp_flow_table_header_fields getRule(int id);
        virtual bool findRule(int id);
};


#endif
