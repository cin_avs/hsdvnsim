#include "SDVNSimQoSClassifier.h"
#include "messages/MessageStructures.h"
#include "messages/StatusMsg_m.h"
#include "messages/VideoRTPMessage_m.h"
#include "messages/ApplicationMsg_m.h"


#ifdef WITH_IPv4
#  include "inet/networklayer/ipv4/IPv4Datagram.h"
#  include "inet/networklayer/ipv4/ICMPMessage_m.h"
#endif
#ifdef WITH_IPv6
#  include "inet/networklayer/ipv6/IPv6Datagram.h"
#  include "inet/networklayer/icmpv6/ICMPv6Message_m.h"
#endif
#ifdef WITH_UDP
#  include "inet/transportlayer/udp/UDPPacket.h"
#endif
#ifdef WITH_TCP_COMMON
#  include "inet/transportlayer/tcp_common/TCPSegment.h"
#endif


namespace inet {

Define_Module(SDVNSimQoSClassifier);

void SDVNSimQoSClassifier::initialize(int stage){/*TODO*/}


void SDVNSimQoSClassifier::handleMessage(cMessage *msg)
{
    std::string classPacket = msg->getClassName();
    std::string typePacket = msg->getFullName();

    if(!classPacket.compare("inet::EthernetIIFrame") == 0){

        Ieee802Ctrl *ctrl = check_and_cast<Ieee802Ctrl*>(msg->removeControlInfo());
        int priority = getPriority(msg);
        ctrl->setUserPriority(priority);
        msg->setControlInfo(ctrl);

    }
    send(msg, "out");
}

int SDVNSimQoSClassifier::getPriority(cMessage *msg)
{
    cPacket *ipData = nullptr;
    int priority;

    string verify;

#ifdef WITH_IPv4
    ipData = dynamic_cast<IPv4Datagram *>(msg);
    if (ipData && dynamic_cast<ICMPMessage *>(ipData->getEncapsulatedPacket()))
        return 0; // ICMP class
#endif

    if (!ipData){
        return 0;
    }

#ifdef WITH_UDP
    UDPPacket *udp = dynamic_cast<UDPPacket *>(ipData->getEncapsulatedPacket());
    if (udp) {
        verify = udp->getFullName();
        if(verify.compare("VideoFrame") == 0){
            VideoRTPMessage *videoPkt = dynamic_cast<VideoRTPMessage *>(udp->getEncapsulatedPacket());
            priority = videoPkt->getQosParameter();

            if(priority > 7){
                priority = 0;
            }
        }else if(verify.compare("AppFrame") == 0){
            ApplicationMsg *appMsg = dynamic_cast<ApplicationMsg *>(udp->getEncapsulatedPacket());
            priority = appMsg->getQosParameter();
            if(priority > 7){
                priority = 0;
            }
        }else{
            priority = 0;
        }
    }
#endif

    return priority;


}

}

