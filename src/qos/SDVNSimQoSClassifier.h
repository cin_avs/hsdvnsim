#ifndef __SDVNSIMQOSCLASSIFIER_H
#define __SDVNSIMQOSCLASSIFIER_H

#include "inet/common/INETDefs.h"
#include "inet/linklayer/common/Ieee802Ctrl.h"

namespace inet {

class SDVNSimQoSClassifier : public cSimpleModule
{
  protected:
    int parameter = 0;
    bool ver = true;
  protected:
    virtual void initialize(int stage) override;
    virtual void handleMessage(cMessage *msg) override;

  protected:
    virtual int getPriority(cMessage *msg);

  public:
    SDVNSimQoSClassifier() {}
};

}

#endif
